var searchData=
[
  ['removeall_0',['removeAll',['../classedu_1_1my_restaurant_1_1service_1_1_menu_service.html#a6058ef87848b64f3b770530d31a7353a',1,'edu::myRestaurant::service::MenuService']]],
  ['removedish_1',['removeDish',['../classedu_1_1my_restaurant_1_1domain_1_1_menu.html#a11ec01a9510ff31137b844c45912a0f6',1,'edu.myRestaurant.domain.Menu.removeDish()'],['../classedu_1_1my_restaurant_1_1service_1_1_menu_service.html#a589271b5b699743f14279c09ffed5078',1,'edu.myRestaurant.service.MenuService.removeDish(String id)']]],
  ['removedishes_2',['removeDishes',['../classedu_1_1my_restaurant_1_1service_1_1_menu_service.html#a2e4134dafd923de3eff75d4a1b11f687',1,'edu::myRestaurant::service::MenuService']]],
  ['reservation_3',['Reservation',['../classedu_1_1my_restaurant_1_1domain_1_1_reservation.html#ae3af0b4088090474f51756c37a810994',1,'edu.myRestaurant.domain.Reservation.Reservation()'],['../classedu_1_1my_restaurant_1_1domain_1_1_reservation.html#ac77dbb6ee060e808eea7eb8054709468',1,'edu.myRestaurant.domain.Reservation.Reservation(String email, String tel, LocalDateTime datetime, int people, int tablenumber)']]],
  ['responsefile_4',['ResponseFile',['../classedu_1_1my_restaurant_1_1message_1_1_response_file.html#a1c84502fab2cf7c886830a102a155d89',1,'edu::myRestaurant::message::ResponseFile']]],
  ['responsemessage_5',['ResponseMessage',['../classedu_1_1my_restaurant_1_1message_1_1_response_message.html#add47fad31a2f8e5ae5d0404fc9a6efd5',1,'edu::myRestaurant::message::ResponseMessage']]]
];
