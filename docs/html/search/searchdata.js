var indexSectionsWithContent =
{
  0: "abcdefghijklmnprstuvw",
  1: "adeijlmrtuw",
  2: "e",
  3: "abdeghijklmnprstuw",
  4: "acdefgijlmprsuv",
  5: "tu",
  6: "f"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Pages"
};

