package edu.myRestaurant.controllerTest;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import edu.myRestaurant.MyRestaurantApplication;
import edu.myRestaurant.domain.Account;
import edu.myRestaurant.jwtutils.JwtRequestModel;
import edu.myRestaurant.jwtutils.JwtResponseModel;
import edu.myRestaurant.jwtutils.TokenManager;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = MyRestaurantApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(locations = "classpath:application-integrationtest.properties")
@DisplayName("Reservation Controller API TEST")
public class ReservationControllerTest {
    

    @LocalServerPort
	private int port;

    HttpHeaders headers = new HttpHeaders();

    @Autowired
    private TestRestTemplate restTemplate;

    private Account registeredAccount;

    @Autowired
    TokenManager tokenManager;

    String token;


    private String getRootUrl() {
        return "http://localhost:" + port;
    }

    


    
    @BeforeEach
    public void setup() {
        registeredAccount= new Account("email", "name", "surname", "333333", "password", "ADMIN");
        registeredAccount.setPassword("password");
        HttpEntity<Account> entity = new HttpEntity<Account>(registeredAccount, headers);
        ResponseEntity<Account> postResponse = restTemplate.exchange(getRootUrl()+"/register",HttpMethod.POST ,entity, Account.class);

        Account account1=postResponse.getBody();
        JwtRequestModel jwtRequest=new JwtRequestModel(account1.getEmail(), "password");
        System.out.println(jwtRequest.toString());
        HttpEntity<JwtRequestModel> entityLogin = new HttpEntity<JwtRequestModel>(jwtRequest, headers);
        ResponseEntity<JwtResponseModel> response = restTemplate.exchange(getRootUrl()+ "/login", HttpMethod.POST, entityLogin, JwtResponseModel.class);  
        
        //LOGIN TOKEN
        this.token=response.getBody().getToken();
        registeredAccount.setId(response.getBody().getId());
        headers.add("Authorization", "Bearer "+ this.token);


    }


    @Test
    public void contextLoads() {
    }



    /**====================================================   REGISTRARTION  ======================================================================= */

    @Test
    public void createReservationTest(){
        Account account= new Account("email", "name", "surname", "333333", "password", "ADMIN");
        account.setPassword("password");
        
        // headers.add("Authorization", "Bearer "+ this.token);
        HttpEntity<Account> entity = new HttpEntity<Account>(account, headers);
        ResponseEntity<String> postResponse = restTemplate.exchange(getRootUrl()+"/manageReservation/createReservation",HttpMethod.POST ,entity, String.class);
        
        
        //assertEquals(postResponse.getStatusCodeValue(), 409);
    }


}
