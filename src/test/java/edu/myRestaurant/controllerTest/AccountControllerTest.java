/**
 * 
 * 
 * to do list :
 * - choose one from junit4 and junit5
 * - set up the code for make single test or fully test suite execution equally
 * - 
 */



package edu.myRestaurant.controllerTest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

// import static org.junit.Assert.assertEquals;
// import static org.junit.Assert.assertNotNull;

import java.util.Collections;

import org.json.JSONException;
import org.junit.jupiter.api.AfterEach;
// import org.json.JSONObject;
// import org.junit.After;
// import org.junit.Before;
// import org.junit.BeforeClass;
// import org.junit.Test;
// import org.junit.experimental.categories.Category;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
// import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.HttpClientErrorException;

import edu.myRestaurant.MyRestaurantApplication;
import edu.myRestaurant.domain.Account;
import edu.myRestaurant.jwtutils.JwtRequestModel;
import edu.myRestaurant.jwtutils.JwtResponseModel;
import edu.myRestaurant.jwtutils.TokenManager;
import edu.myRestaurant.service.AccountService;

// @Category(Integration.class)
// @RunWith(SpringRunner.class)
@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = MyRestaurantApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(locations = "classpath:application-integrationtest.properties")
@DisplayName("Account Controller API TEST")
public class AccountControllerTest {


    @LocalServerPort
	private int port;

    HttpHeaders headers = new HttpHeaders();

    @Autowired
    private TestRestTemplate restTemplate;

    private Account registeredAccount;

    @Autowired
    AccountService accountService;

    @Autowired
    TokenManager tokenManager;

    String token;

 
  
 
    private String getRootUrl() {
        return "http://localhost:" + port;
    }

    


    
    @BeforeEach
    public void setup() {
        registeredAccount= new Account("email", "name", "surname", "333333", "password", "ADMIN");
        registeredAccount.setPassword("password");
        HttpEntity<Account> entity = new HttpEntity<Account>(registeredAccount, headers);
        ResponseEntity<Account> postResponse = restTemplate.exchange(getRootUrl()+"/register",HttpMethod.POST ,entity, Account.class);

        Account account1=postResponse.getBody();
        JwtRequestModel jwtRequest=new JwtRequestModel(account1.getEmail(), "password");
        System.out.println(jwtRequest.toString());
        HttpEntity<JwtRequestModel> entityLogin = new HttpEntity<JwtRequestModel>(jwtRequest, headers);
        ResponseEntity<JwtResponseModel> response = restTemplate.exchange(getRootUrl()+ "/login", HttpMethod.POST, entityLogin, JwtResponseModel.class);  
        
        //LOGIN TOKEN
        this.token=response.getBody().getToken();
        registeredAccount.setId(response.getBody().getId());
        headers.add("Authorization", "Bearer "+ this.token);


    }
     
    @Test
    public void contextLoads() {
    }



    /**====================================================   REGISTRARTION  ======================================================================= */

    @Test
    public void registrerAccountAlreadyExistTest(){
        Account account= new Account("email", "name", "surname", "333333", "password", "ADMIN");
        account.setPassword("password");
        
        // headers.add("Authorization", "Bearer "+ this.token);
        HttpEntity<Account> entity = new HttpEntity<Account>(account, headers);
        ResponseEntity<String> postResponse = restTemplate.exchange(getRootUrl()+"/register",HttpMethod.POST ,entity, String.class);
        
        
        assertEquals(postResponse.getStatusCodeValue(), 409);
    }




    @Test
    public void registrerAaccountBadRequestTest(){
        Account account= new Account(null, "name", "surname", "333333", "password", "ADMIN");
        account.setPassword("password");
        HttpEntity<Account> entity = new HttpEntity<Account>(account, headers);
        ResponseEntity<String> postResponse = restTemplate.exchange(getRootUrl()+"/register",HttpMethod.POST ,entity, String.class);
       
       
        assertEquals(postResponse.getStatusCodeValue(), 400);
    }





    @Test
    public void RegistrationLoginAccountTest() {

        //REGISTRER API
        Account newAccount= new Account("newAccount", "name", "surname", "333333", "password", "ADMIN");
        newAccount.setPassword("password");
        HttpEntity<Account> entity = new HttpEntity<Account>(newAccount, headers);
        ResponseEntity<Account> postResponse = restTemplate.exchange(getRootUrl()+"/register",HttpMethod.POST ,entity, Account.class);

        //ASSERT REGISTRATION
        assertEquals(postResponse.getStatusCodeValue(), 201 );
        assertEquals(postResponse.getBody().getEmail(),newAccount.getEmail());

        //LOGIN API
        Account accountCreated=postResponse.getBody();
        JwtRequestModel jwtRequest=new JwtRequestModel(accountCreated.getEmail(), "password");
        System.out.println(jwtRequest.toString());
        HttpEntity<JwtRequestModel> entityLogin = new HttpEntity<JwtRequestModel>(jwtRequest, headers);
        ResponseEntity<JwtResponseModel> loginResponse = restTemplate.exchange(getRootUrl()+ "/login", HttpMethod.POST, entityLogin, JwtResponseModel.class);  
        

        //ASSERT LOGIN
        assertEquals(loginResponse.getStatusCodeValue(), 200);
        assertNotNull(loginResponse.getBody().getId());
        assertNotNull(loginResponse.getBody().getToken());
        assertEquals(tokenManager.getUsernameFromToken(loginResponse.getBody().getToken()),newAccount.getEmail());
    }




    /**========================================================   GET   =================================================================== */

    @Test
    public void testGetAllAccount() {

        headers.add("Authorization", "Bearer "+ this.token);
        HttpEntity<String> entity1 = new HttpEntity<String>(null, headers);
        ResponseEntity<String> response1 = restTemplate.exchange(getRootUrl()+ "/manageAccount/getAllAccount", HttpMethod.GET, entity1, String.class);  
        
        
        assertEquals(response1.getStatusCodeValue(), 200);
        assertNotNull(response1.getBody());
    }




    @Test
    public void getAccountByIdTest() {

        headers.add("Authorization", "Bearer "+ this.token);
        HttpEntity<String> entity = new HttpEntity<String>(null, headers);
        ResponseEntity<Account> accountByID = restTemplate.exchange(getRootUrl()+"/manageAccount/getAccount/"+this.registeredAccount.getId(),HttpMethod.GET ,entity, Account.class);
    

        assertNotNull(accountByID);
        assertEquals(accountByID.getStatusCodeValue(), 200 );
        assertEquals(accountByID.getBody().getId(),this.registeredAccount.getId());
    }


    @Test
    public void getAccountByIdNotFoundTest() {
        Account account= new Account("nuovo", "name", "surname", "333333", "password", "ADMIN");
        account.setPassword("password");
        account.setId("id");
        
        headers.add("Authorization", "Bearer "+ this.token);
        HttpEntity<String> entity = new HttpEntity<String>(null, headers);
        ResponseEntity<String> accountByID = restTemplate.exchange(getRootUrl()+"/manageAccount/getAccount/"+account.getId(),HttpMethod.GET ,entity, String.class);
    

        assertNotNull(accountByID);
        assertEquals(accountByID.getStatusCodeValue(), 404 );

    }






/**====================================================================    UPDATE   ======================================================= */




    @Test
    public void updateAccountTest() {

        headers.add("Authorization", "Bearer "+this.token );

        // the object's field updating 
        this.registeredAccount.setSurname("Francesco");

        HttpEntity<Account> entityPatch = new HttpEntity<Account>(this.registeredAccount, headers);
        ResponseEntity<Account> patchResponse = restTemplate.exchange(getRootUrl()+"/manageAccount/updateAccount/"+this.registeredAccount.getId(),HttpMethod.PATCH,entityPatch, Account.class);
        
        
        assertEquals(patchResponse.getStatusCodeValue(), 200 );
        assertEquals(patchResponse.getBody().getSurname(),this.registeredAccount.getSurname());

    }



/**=============================================================   DELETE  ============================================================== */


    @AfterEach
    public void testDeleteAccount() {

        headers.add("Authorization", "Bearer "+this.token );
        HttpEntity<Account> entity1 = new HttpEntity<Account>(null, headers);
        ResponseEntity<String> postResponse1 = restTemplate.exchange(getRootUrl()+"/manageAccount/deleteAccount/"+ this.registeredAccount.getId(),HttpMethod.DELETE ,entity1, String.class);
        assertEquals(postResponse1.getStatusCodeValue(), 200 );
        assertNotNull(postResponse1);
    }
}