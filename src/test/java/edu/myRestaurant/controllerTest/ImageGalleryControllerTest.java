package edu.myRestaurant.controllerTest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.List;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

// import static org.junit.Assert.assertEquals;
// import static org.junit.Assert.assertNotNull;

// import org.junit.Test;
// import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.HttpClientErrorException;

import edu.myRestaurant.MyRestaurantApplication;
import edu.myRestaurant.domain.Account;
import edu.myRestaurant.domain.ImageGallery;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = MyRestaurantApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(locations = "classpath:application-integrationtest.properties")
@DisplayName("Image Gallery Controller API TEST")
public class ImageGalleryControllerTest {


     @Autowired
     private TestRestTemplate restTemplate;

     @LocalServerPort
     private int port;

     HttpHeaders headers = new HttpHeaders();

     private String getRootUrl() {
         return "http://localhost:" + port;
     }

     @Test
     public void contextLoads() {

     }

    
     @Test
     public void getAllGalleryImage(){
        
        HttpEntity<Account> entity = new HttpEntity<Account>(null, headers);
        ResponseEntity<List> postResponse = restTemplate.exchange(getRootUrl()+"/gallery/getAllGallery",HttpMethod.GET ,entity, List.class);

        assertEquals(postResponse.getStatusCodeValue(), 200);
     }


   
}