# Frontend

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 13.3.0.


# Angular Dependecies 
This command will update the latest versione of angular  `ng update @angular/cli @angular/core` but if you want to upgrade to specific version then you have to add @version in dependency as belo `ng update @angular/cli@7.0.0`


# Multipage app
Create Component login , home , manageAccount, make sure that are insert into app.module.ts 
Configure app routing
```ts
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { ManageAccountComponent } from './manage-account/manage-account.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'login', component: LoginComponent },
  { path: 'manageAccount', component: ManageAccountComponent },

  // otherwise redirect to home
  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
```
Add app routing module to app module. 
The routerLink Angular directive enables navigation between different routes, and the `<router-outlet></router-outlet>` Angular directive displays the component for the current route.



# External Api

Google Maps `npm install @angular/google-maps`  , When the installation is finished, we must add the Angular module GoogleMapsModule to the import declaration. Insert the component in the page html `<google-map></google-map>`

OpenWeatherApi create a component weather-card and a service to fetch the api .




## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via a platform of your choice. To use this command, you need to first add a package that implements end-to-end testing capabilities.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
