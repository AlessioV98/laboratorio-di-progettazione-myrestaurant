import {location} from './location';
// just an interface for type safety.
export interface marker {
	location:location
	label: string;
	title: string;
  	description:string;
  	url:string;
}