export interface imageGallery{
    id:string;
    image: string,
	thumbImage: string,
	alt: string,
	title: string
}
