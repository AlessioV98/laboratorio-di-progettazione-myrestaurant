export interface Account {
    email: string;
    name: string;
    password: string;
    surname:string;
    role: string;
    tel: string;
    id:string;
}

