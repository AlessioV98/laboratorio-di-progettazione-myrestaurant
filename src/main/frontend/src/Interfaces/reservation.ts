export interface Reservation {
    id: string
    email: string;
    datetime: string;
    tel: string;
    people: number;
    tablenumber: number;


}
