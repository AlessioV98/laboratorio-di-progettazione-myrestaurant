import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, Observable, retry, throwError } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class ManageAdminService {

  endPoint = 'http://localhost:8080';


  constructor(private http: HttpClient) { }

  createMenu(formData: any):Observable<any>{
    return this.http.post<any>(this.endPoint+ '/manageMenu/getAllMenu', formData).pipe(retry(1),
    catchError(this.httpError)
    )
  }


  httpError(error:any) {
    let msg = '';
    if(error.error instanceof ErrorEvent) {
      // client side error
      msg = error.error.message;
    } else {
      // server side error
      msg = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    console.log(msg);
    return throwError(msg);
  }


  
}
