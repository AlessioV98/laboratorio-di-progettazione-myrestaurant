import { TestBed, inject } from "@angular/core/testing";
import {
  HttpClientTestingModule,
  HttpTestingController
} from "@angular/common/http/testing";

import { ManageAccountService } from './manage-account.service';
import { Account } from 'src/Interfaces/account';

describe('ManageAccountService', () => {
  let service: ManageAccountService;
  let httpTestingController: HttpTestingController;
  let dataAccessService: ManageAccountService;
  let baseUrl = "http://localhost:8080";
  let account: Account;
  

  beforeEach(() => {
    TestBed.configureTestingModule({imports: [HttpClientTestingModule]});
    service = TestBed.inject(ManageAccountService);
    httpTestingController = TestBed.get(HttpTestingController);
    account = {
      email:"admin",
      name:"admin",
      password:"admin",
      surname:"admin",
      role:"ADMIN",
      tel:"0331",
      id:""
    };
  });

  beforeEach(inject(
    [ManageAccountService],
    (service: ManageAccountService) => {
      dataAccessService = service;
    }
  ));


  it('should be created', () => {
    expect(service).toBeTruthy();
  });


  it("should return all Account", () => {
    let result: Account[]=[];
    dataAccessService.getAllAccount().subscribe((t) => {
      result = t;
    });
    const req = httpTestingController.expectOne({
      method: "GET",
      url: baseUrl+"/manageAccount/getAllAccount"
    });
    req.flush([account]);
    expect(result[0]).toEqual(account);
  });


  it("api registration", () => {
    let result: Account[]=[];
    dataAccessService.register(account).subscribe((t) => {
      result = t;
    });
    const req = httpTestingController.expectOne({
      method: "POST",
      url: baseUrl+"/register"
    });
    req.flush([account]);
    expect(result[0]).toEqual(account);
  });



});
