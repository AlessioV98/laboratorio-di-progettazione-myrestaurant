import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, Observable, retry, throwError } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class MenuCardService {

  httpHeader = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Accept': '*/*'
    })
  }  

  endPoint = 'http://localhost:8080';


  constructor(private http: HttpClient) { }


  getAccounts(): Observable<any> {
    return this.http.get<any>(this.endPoint + '/manageMenu/getAllMenu').pipe(retry(1),
          catchError(this.httpError)
    )
  }


  


  httpError(error:any) {
    let msg = '';
    if(error.error instanceof ErrorEvent) {
      // client side error
      msg = error.error.message;
    } else {
      // server side error
      msg = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    console.log(msg);
    return throwError(msg);
  }



}
