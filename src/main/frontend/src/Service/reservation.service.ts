import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Reservation } from 'src/Interfaces/reservation';

@Injectable({
  providedIn: 'root'
})
export class ReservationService {

  api: string ="http://localhost:8080";
  

  constructor(private http: HttpClient) { }

  ngOnInit(): void {
  }



  createReservation(datetime : string, people : number){
    const params = new HttpParams()
          .set('datetime', datetime)
          .set('people', people);
    return this.http.post<any>(this.api+"/manageReservation/createReservation",JSON.stringify({
      datetime: datetime,
      people: people}), {
      headers: {
          'Content-type': 'application/json',
          'Accept': 'application/json'
      }});
  }

  getAllReservationsByEmail(email : string){
    return this.http.get<any>(this.api+"/manageReservation/getAllReservationsByEmail/"+email,{headers: {
      'Content-type': 'application/json',
      'Accept': 'application/json'
  }});
  }

  getAllReservation(){
    return this.http.get<any>(this.api+"/manageReservation/getAllReservation",{headers: {
      'Content-type': 'application/json',
      'Accept': 'application/json'
  }});
  }
  getReservationByDay(date : string){
  return this.http.get<any>(this.api+"/manageReservation/getReservationByDay/"+date, {
    headers: {
        'Content-type': 'application/json',
        'Accept': 'application/json'
    }});
  }

  deleteReservation(id: string){
    return this.http.delete<any>(this.api+"/manageReservation/deleteReservation/"+id, {
      headers: {
          'Content-type': 'application/json',
          'Accept': 'application/json',
          'Access-Control-Allow-Origin':'*'
      }});
  }

  addTableToReservation(id : string, tablenumber : number){
    const params = new HttpParams()
          .set('id', id)
          .set('tablenumber', tablenumber);
    return this.http.post<any>(this.api+"/manageReservation/addTableToReservation",JSON.stringify({
      id: id,
      tablenumber: tablenumber}), {
      headers: {
          'Content-type': 'application/json',
          'Accept': 'application/json'
      }});
  }


  updateReservation(reservation:Reservation){
    return this.http.patch<any>(this.api+"/manageReservation/updateReservation/"+reservation.id,reservation , {
      headers: {
          'Content-type': 'application/json',
          'Accept': 'application/json',
          'Access-Control-Allow-Origin':'*'
      }});
  }
}
