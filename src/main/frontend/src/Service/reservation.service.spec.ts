import { TestBed, inject } from "@angular/core/testing";
import {
  HttpClientTestingModule,
  HttpTestingController
} from "@angular/common/http/testing";

import { ReservationService } from './reservation.service';
import { Reservation } from 'src/Interfaces/reservation';

describe('ManagereservationService', () => {
  let service: ReservationService;
  let httpTestingController: HttpTestingController;
  let dataAccessService: ReservationService;
  let baseUrl = "http://localhost:8080";
  let reservation: Reservation;
  

  beforeEach(() => {
    TestBed.configureTestingModule({imports: [HttpClientTestingModule]});
    service = TestBed.inject(ReservationService);
    httpTestingController = TestBed.get(HttpTestingController);
    reservation = {
      id:"1",
      email: "user",
      datetime: "",
      tel: "tel",
      people: 0,
      tablenumber:1,
    };
  });

  beforeEach(inject(
    [ReservationService],
    (service: ReservationService) => {
      dataAccessService = service;
    }
  ));


  it('should be created', () => {
    expect(service).toBeTruthy();
  });


  it("should return all reservation", () => {
    let result: Reservation[]=[];
    dataAccessService.getAllReservation().subscribe((t) => {
      result = t;
    });
    const req = httpTestingController.expectOne({
      method: "GET",
      url: baseUrl+"/manageReservation/getAllReservation"
    });
    req.flush([reservation]);
    expect(result[0]).toEqual(reservation);
  });


  it("api create reservation", () => {
    reservation = {
      id:"1",
      email: "user",
      datetime: "",
      tel: "tel",
      people: 0,
      tablenumber:2
    };
    let result: Reservation[]=[];
    dataAccessService.createReservation(reservation.datetime, reservation.people).subscribe((t) => {
      result = t;
    });
    const req = httpTestingController.expectOne({
      method: "POST",
      url: baseUrl+"/manageReservation/createReservation"
    });
    req.flush([reservation]);
    expect(result[0]).toEqual(reservation);
  });



});
