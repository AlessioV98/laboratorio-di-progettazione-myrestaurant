import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Account } from 'src/Interfaces/account';

@Injectable({
  providedIn: 'root'
})
export class ManageAccountService {

  api: string ="http://localhost:8080";
  

  constructor(private http: HttpClient) { }

  ngOnInit(): void {
  }


  login(account:Account){
    return this.http.post<any>(this.api+"/login",account,{headers: {
      'Content-type': 'application/json',
      'Accept': 'application/json',
      }});
  }


  register(account:Account){
    return this.http.post<any>(this.api+"/register",account,{headers: {
      'Content-type': 'application/json',
      'Accept': 'application/json'
  }});
  }


  getAllAccount(){
    return this.http.get<any>(this.api+"/manageAccount/getAllAccount",{headers: {
      'Content-type': 'application/json',
      'Accept': 'application/json'
  }});
  }
  getAccount(id: string){
    return this.http.get<any>(this.api+"/manageAccount/getAccount/"+id,{headers: {
      'Content-type': 'application/json',
      'Accept': 'application/json'
  }});
  }


  deleteAccount(account:Account){
    return this.http.delete<any>(this.api+"/manageAccount/deleteAccount/"+account.id, {
      headers: {
          'Content-type': 'application/json',
          'Accept': 'application/json',
          'Access-Control-Allow-Origin':'*'
      }});
  }




  updateAccount(account:Account){
    return this.http.patch<any>(this.api+"/manageAccount/updateAccount/"+account.id,account , {
      headers: {
          'Content-type': 'application/json',
          'Accept': 'application/json',
          'Access-Control-Allow-Origin':'http://localhost:4200/'
      }});
  }
}
