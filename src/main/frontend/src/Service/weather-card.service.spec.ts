import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';

import { WeatherCardService } from './weather-card.service';

describe('WeatherCardService', () => {
  let service: WeatherCardService;
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({imports: [HttpClientTestingModule]});
    httpTestingController = TestBed.get(HttpTestingController);
    service = TestBed.inject(WeatherCardService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
