import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, Observable, retry, throwError } from 'rxjs';
import { imageGallery } from 'src/Interfaces/imageGallery';

@Injectable({
  providedIn: 'root'
})
export class GalleryService {

  api: string ="http://localhost:8080";

  constructor(private http: HttpClient) { }



  /**
   * This method is used to fetch a get all of image gallery
   * @returns imageGallery[]
   */
  getAllGalleryImage(){
    return this.http.get<imageGallery[]>(this.api+"/gallery/getAllGallery",{headers: {
      'Content-type': 'application/json',
      'Accept': 'application/json'
  }});
  }

  /**
   * This method is used to fetch backend API to post form-data request di multipartfile
   * @param formData FormData
   * @returns jsonResponse
   */
  createGalleryImage(formData: FormData):Observable<any>{
    return this.http.post<FormData>(this.api+ '/gallery/createImageGallery', formData).pipe(retry(1),
    catchError(this.httpError)
    )
  }

  /**
   * This method is used to fetch backend API to delete a ImageGallery 
   * use id with path-parameter
   * @param image 
   * @returns jsonResponse
   */
  deleteImageGallery(image:imageGallery){
    return this.http.delete<any>(this.api+"/gallery/deleteImageGallery/"+image.id, {
      headers: {
          'Content-type': 'application/json',
          'Accept': 'application/json',
          'Access-Control-Allow-Origin':'*'
      }});
  }


  /**
   * This method is used for catch error event 
   * @param error 
   * @returns 
   */
  httpError(error:any) {
    let msg = '';
    if(error.error instanceof ErrorEvent) {
      // client side error
      msg = error.error.message;
    } else {
      // server side error
      msg = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    console.log(msg);
    return throwError(msg);
  }
  
}
