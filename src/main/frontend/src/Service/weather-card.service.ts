import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, Observable, retry, throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class WeatherCardService {


  url: string="https://api.openweathermap.org/data/2.5/onecall?lat=45.51686960789838&lon=8.750322514022459&appid=fe30841c403c07c4ef1586c675af0548&units=metrics&lang=en";

  constructor(private httpClient: HttpClient) { }

  getWeather(): Observable<any> {
    return this.httpClient.get<any>(this.url).pipe(retry(1),
          catchError(this.httpError)
    )
  }


  httpError(error:any) {
    let msg = '';
    if(error.error instanceof ErrorEvent) {
      // client side error
      msg = error.error.message;
    } else {
      // server side error
      msg = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    console.log(msg);
    return throwError(msg);
  }

}
