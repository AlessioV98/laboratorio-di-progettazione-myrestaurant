import { Component, OnInit } from '@angular/core';
import { Account } from 'src/Interfaces/account';
import { Reservation } from 'src/Interfaces/reservation';
import { ManageAccountService } from 'src/Service/manage-account.service';
import { ReservationService } from 'src/Service/reservation.service';
import { TokenStorageService } from 'src/Service/token-storage.service';

@Component({
  selector: 'app-manage-reservation',
  templateUrl: './manage-reservation.component.html',
  styleUrls: ['./manage-reservation.component.css']
})
export class ManageReservationComponent implements OnInit {

  reservations!: Reservation[];
  account: Account={ email: "", name:"", password: "", surname: "", role:"", tel: "", id:""};
  inEdit: boolean = false;
  res: Reservation = {id:"",email: "", datetime: "", tel: "", people: 0, tablenumber: 0}

  constructor(public accountService:ManageAccountService, public reservationService:ReservationService, public tokenService:TokenStorageService) { }

  ngOnInit(): void {
      this.accountService.getAccount(this.tokenService.getUser()).subscribe((res) =>{
        this.account.email = res.email
        this.account.tel = res.tel
        this.reservationService.getAllReservationsByEmail(res.email).subscribe((reser)=>{
          this.reservations = reser.reservations
      });
  
    });
    //this.getAllAccount();
  //   this.formdata = new FormGroup({
  //     emailid: new FormControl("angular@gmail.com"),
  //     passwd: new FormControl("abcd1234")
  //  });
  }

  onSubmit(): void {
    this.reservationService.updateReservation(this.res).subscribe((res) =>{
      alert(res.response)
      if(res.response == "Reservation updated")
        window.location.reload();
    });

  }
  edit(id: string, datetime: string, people : number) : void{
    this.inEdit = true;
    this.res.id = id;
    this.res.datetime = datetime;
    this.res.people = people;
    this.res.tel = this.account.tel;
    this.res.email = this.account.email;
    //window.location.reload();
  } 

  delete(id : string) : void{
    this.reservationService.deleteReservation(id).subscribe((res) =>{
      alert("Reservation deleted");
      
    });
    window.location.reload();
  } 
}
