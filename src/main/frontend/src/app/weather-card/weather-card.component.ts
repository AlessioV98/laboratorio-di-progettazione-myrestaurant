import { Component, OnInit } from '@angular/core';
import { WeatherCardService } from 'src/Service/weather-card.service';

@Component({
  selector: 'app-weather-card',
  templateUrl: './weather-card.component.html',
  styleUrls: ['./weather-card.component.css']
})
export class WeatherCardComponent implements OnInit {

  days!:any[];

  imgageurl!: String[];

  constructor(public weatherService:WeatherCardService) { }

  ngOnInit() {
    this.fetchUsers()
  }

  fetchUsers() {
    this.weatherService.getWeather().subscribe((res) => {
          
      this.days=res.daily.slice(0,3);
      console.log("this.days")
      console.log(this.days)
    })    
  }

}
