import { Component, OnInit } from '@angular/core';
import { Account } from 'src/Interfaces/account';
import { ManageAccountService } from 'src/Service/manage-account.service';
import { TokenStorageService } from 'src/Service/token-storage.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {

  account: Account={ email: "", name:"", password: "", surname: "", role:"", tel: "", id:""};

  constructor(public accountService:ManageAccountService, public tokenService:TokenStorageService) { }

  ngOnInit(): void {
      this.accountService.getAccount(this.tokenService.getUser()).subscribe((res) =>{
      this.account.id = res.id;
      this.account.email = res.email;
      this.account.name = res.name;
      this.account.surname = res.surname;
      this.account.password = res.password;
      this.account.role = res.role;
      this.account.tel = res.tel;
    });
  }
}
