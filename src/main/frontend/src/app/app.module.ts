import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AgmCoreModule } from '@agm/core';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { ManageAccountComponent } from './manage-account/manage-account.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { RegistrationComponent } from './registration/registration.component';
import { ManageAdminComponent } from './manage-admin/manage-admin.component';
import { MenusCardComponent } from './menus-card/menus-card.component';
import { WeatherCardComponent } from './weather-card/weather-card.component';
import { NavbarComponent } from './navbar/navbar.component';
import { AgmDirectionModule } from 'agm-direction'; 
import { JwtInterceptor } from './jwt-interceptor';
import { ReservationComponent } from './reservation/reservation.component';
import { ManageReservationComponent } from './manage-reservation/manage-reservation.component';

import { ManageTablesComponent } from './manage-tables/manage-tables.component';
import { ManageReservationAdminComponent } from './manage-reservation-admin/manage-reservation-admin.component';
import { NgImageSliderModule } from 'ng-image-slider';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LoginComponent,
    ManageAccountComponent,
    SidebarComponent,
    RegistrationComponent,
    ManageAdminComponent,
    MenusCardComponent,
    WeatherCardComponent,
    NavbarComponent,
    ReservationComponent,
    ManageReservationComponent,
    ManageTablesComponent,
    ManageReservationAdminComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    AgmCoreModule.forRoot({
      // please get your own API key here:
      // https://developers.google.com/maps/documentation/javascript/get-api-key?hl=en
      apiKey: 'AIzaSyCCx7vslutL_K_m84bbyDYnmJrfV3IGc5s'
    }),
    AgmDirectionModule,     // agm-direction
    NgImageSliderModule
  ],
  providers: [LoginComponent,
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true }],
  bootstrap: [AppComponent]

})
export class AppModule { }
