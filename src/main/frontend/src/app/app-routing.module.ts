import { NgModule } from '@angular/core';
import { Router, RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { ManageAccountComponent } from './manage-account/manage-account.component';
import { ManageAdminComponent } from './manage-admin/manage-admin.component';
import { RegistrationComponent } from './registration/registration.component';
import { ReservationComponent } from './reservation/reservation.component';
import { ManageReservationComponent } from './manage-reservation/manage-reservation.component';
import { ManageTablesComponent } from './manage-tables/manage-tables.component';
import { ManageReservationAdminComponent } from './manage-reservation-admin/manage-reservation-admin.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'login', component: LoginComponent },
  { path: 'manageAccount', component: ManageAccountComponent },
  { path: 'register', component: RegistrationComponent },
  { path: 'createReservation', component: ReservationComponent },
  { path: 'manageReservation', component: ManageReservationComponent },
  { path: 'manageAdmin', component: ManageAdminComponent },
  { path: 'reservations', component: ManageReservationAdminComponent },
  { path: 'manageTables', component: ManageTablesComponent },

  // otherwise redirect to home
  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
  constructor(private router: Router){}
}
