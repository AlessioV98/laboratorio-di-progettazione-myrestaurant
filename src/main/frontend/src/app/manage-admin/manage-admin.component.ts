import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Component, createPlatform, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { imageGallery } from 'src/Interfaces/imageGallery';
import { GalleryService } from 'src/Service/gallery.service';
import { ManageAdminService } from 'src/Service/manage-admin.service';

@Component({
  selector: 'app-manage-admin',
  templateUrl: './manage-admin.component.html',
  styleUrls: ['./manage-admin.component.css']
})
export class ManageAdminComponent implements OnInit {

  uploadForm!: FormGroup;
  api: string= "http://localhost:8080/manageMenu/upload" ;
  imgCollection!: imageGallery[] ;


  constructor(
    public fb: FormBuilder,
    private http: HttpClient,
    private menuService:ManageAdminService,
    private galleryService: GalleryService
  ) {
    
    
  }

  ngOnInit() {
    this.uploadForm = this.fb.group({
      file: ['']
    });
    this.getAllGalleryImages()
		
	}


  deleteImage(image:imageGallery){
    this.galleryService.deleteImageGallery(image).subscribe((res)=>{
      alert(res.response)
    })
  }



	getAllGalleryImages(){
		this.galleryService.getAllGalleryImage().subscribe((res)=>{
			console.log("------------------res api------------------------")
			console.log(res)
			this.imgCollection=res;
			console.log("---------------------gallery----------------------")
		console.log(this.imgCollection)
		for(let i=0;i<this.imgCollection.length;i++){
			this.imgCollection[i].image="./assets/images/gallery/"+this.imgCollection[i].image;
			this.imgCollection[i].thumbImage=this.imgCollection[i].image;
			console.log("------------------src url---------------")
			console.log(this.imgCollection[i].image)
		}
		});
		
	}

  onSubmit(event :any) {
    // this.uploadForm.get('titolo')?.setValue(event.target.titolo.value);
    // this.uploadForm.get('descrizione')?.setValue(event.target.descrizione.value);
    const formData = new FormData();
    formData.append('file', this.uploadForm.get('file')?.value);
    // formData.append('titolo',this.uploadForm.get('titolo')?.value);
    // formData.append('descrizione',this.uploadForm.get('descrizione')?.value);
    // console.log(formData.get('titolo'));
    this.galleryService.createGalleryImage(formData).subscribe((res) =>{
      alert(res.response)
    })
  }


  onFileSelect(event:any) {
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.uploadForm.get('file')?.setValue(file);
      console.log('onfileselect')
      console.log(this.uploadForm.get('file'))
    }
  }
 }
