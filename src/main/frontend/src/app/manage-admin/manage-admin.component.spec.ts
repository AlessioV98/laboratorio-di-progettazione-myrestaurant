import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { MenuCardService } from 'src/Service/menu-card.service';

import { ManageAdminComponent } from './manage-admin.component';

describe('ManageAdminComponent', () => {
  let component: ManageAdminComponent;
  let fixture: ComponentFixture<ManageAdminComponent>;
  let httpTestingController: HttpTestingController;
  let fb:FormBuilder;

  beforeEach(async () => {
    TestBed.configureTestingModule({imports: [HttpClientTestingModule]});
    await TestBed.configureTestingModule({
      declarations: [ ManageAdminComponent ],
      imports :[ReactiveFormsModule]
    })
    .compileComponents();
    fb = TestBed.inject<FormBuilder>(FormBuilder);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
