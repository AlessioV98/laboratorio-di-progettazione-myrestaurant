import { Component, OnInit } from '@angular/core';
import { Account } from 'src/Interfaces/account';
import { Reservation } from 'src/Interfaces/reservation';
import { ManageAccountService } from 'src/Service/manage-account.service';
import { ReservationService } from 'src/Service/reservation.service';
import { TokenStorageService } from 'src/Service/token-storage.service';

@Component({
  selector: 'app-manage-reservation-admin',
  templateUrl: './manage-reservation-admin.component.html',
  styleUrls: ['./manage-reservation-admin.component.css']
})
export class ManageReservationAdminComponent implements OnInit {

  reservations!: Reservation[];
  account: Account={ email: "", name:"", password: "", surname: "", role:"", tel: "", id:""};
  res: Reservation = {id:"",email: "", datetime: "", tel: "", people: 0, tablenumber: 0}

  constructor(public accountService:ManageAccountService, public reservationService:ReservationService, public tokenService:TokenStorageService) { }

  ngOnInit(): void {
  this.reservationService.getAllReservation().subscribe((res)=>{
      this.reservations = res.reservations
  });
    //this.getAllAccount();
  //   this.formdata = new FormGroup({
  //     emailid: new FormControl("angular@gmail.com"),
  //     passwd: new FormControl("abcd1234")
  //  });
  }

  onSubmit(): void {
    

  }

  delete(id : string) : void{
    this.reservationService.deleteReservation(id).subscribe((res) =>{
      alert("Reservation deleted");
      
    });
    window.location.reload();
  } 
}
