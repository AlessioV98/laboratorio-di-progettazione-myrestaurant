import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { Account } from 'src/Interfaces/account';
import { Reservation } from 'src/Interfaces/reservation';
import { ManageAccountService } from 'src/Service/manage-account.service';
import { ReservationService } from 'src/Service/reservation.service';
import { TokenStorageService } from 'src/Service/token-storage.service';

@Component({
  selector: 'app-reservation',
  templateUrl: './reservation.component.html',
  styleUrls: ['./reservation.component.css']
})
export class ReservationComponent implements OnInit {
  
  formdata!: FormGroup;
  reservations!: Reservation[];
  newReservation: Reservation={id: "", email: "", datetime: "", tel: "", people: 0, tablenumber: 0};
  account: Account={ email: "", name:"", password: "", surname: "", role:"", tel: "", id:""};

  constructor(public reservationService:ReservationService,public accountService:ManageAccountService, public tokenService:TokenStorageService, private router:Router) { }

  ngOnInit(): void {

  }
  onSubmit():void{
    
    this.reservationService.createReservation(this.newReservation.datetime,this.newReservation.people).subscribe((res) =>{
      alert(res.response)
      if(res.response == "Reservation created")
        this.router.navigate(['/']);
    });
  }

//   getAllReservation(){
//     this.reservationService.getAllReservation().subscribe((res) => {
          
//       this.reservations=res.reservations
//       console.log("res-accounts")
//       console.log(res.reservations)
//       console.log(this.reservations)
//           // console.log(data.hasOwnProperty("menu"));
//           // this.accounts=data;
//           // console.log(this.accounts);
//     }) 
//   }

//   createReservation(){
//     console.log("createReservation")
//     console.log(this.newReservation)
//     this.reservationService.createReservation(this.newReservation).subscribe((res)=>{
//       alert(res.response)

//     })
//   }

//   updateReservation(){
//     console.log("updateReservation")
//     console.log(this.updatedReservation)
//     this.reservationService.updateReservation(this.updatedReservation).subscribe((res)=>{
//       alert(res.response)

//     })
//   }

//   deleteReservation(){
//     console.log("entra in delete Reservation")
//     this.reservationService.deleteReservation(this.newReservation).subscribe((res)=>{
//       alert(res.response)

//     })
//   }

//   onClickSubmit(data:any) {
    
//     this.newReservation.email=data.email;
//     this.newReservation.datetime=data.datetime;
//     this.newReservation.tel=data.tel;
//     this.newReservation.people=data.people;
//     console.log(this.newReservation)
//     this.createReservation
//  }

}
