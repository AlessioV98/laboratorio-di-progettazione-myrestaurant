import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Account } from 'src/Interfaces/account';
import { ManageAccountService } from 'src/Service/manage-account.service';
import { TokenStorageService } from 'src/Service/token-storage.service';
const TOKEN_KEY = 'auth-token';
const USER_KEY = 'auth-user';


@Component({
  selector: 'app-manage-account',
  templateUrl: './manage-account.component.html',
  styleUrls: ['./manage-account.component.css']
})
export class ManageAccountComponent implements OnInit {
  
  formdata!: FormGroup;
  accounts!: Account[];
  account: Account={ email: "", name:"", password: "", surname: "", role:"", tel: "", id:""};
  updatedAccount: Account={ email: "", name:"", password: "", surname: "", role:"", tel: "", id:""};
  displayedPassword! : string;

  constructor(public accountService:ManageAccountService, public tokenService:TokenStorageService) { }

  ngOnInit(): void {
      this.accountService.getAccount(this.tokenService.getUser()).subscribe((res) =>{
      this.account.id = res.id;
      this.account.email = res.email;
      this.account.name = res.name;
      this.account.surname = res.surname;
      this.account.password = res.password;
      this.displayedPassword = res.password.substring(0,10);
      this.account.role = res.role;
      this.account.tel = res.tel;
    });
    //this.getAllAccount();
  //   this.formdata = new FormGroup({
  //     emailid: new FormControl("angular@gmail.com"),
  //     passwd: new FormControl("abcd1234")
  //  });
  }

  onSubmit(): void {
    this.accountService.updateAccount(this.account).subscribe((res) =>{
    alert("Account: "+res.email+" Updated")
    this.reloadPage();
  }
  );
}
reloadPage(): void {
  window.location.reload();
}


  getAllAccount(){
    this.accountService.getAllAccount().subscribe((res) => {
          
      this.accounts=res.accounts
      console.log("res-accounts")
      console.log(res.accounts)
      console.log(this.accounts)
          // console.log(data.hasOwnProperty("menu"));
          // this.accounts=data;
          // console.log(this.accounts);
    }) 
  }


  // updateAccount(){
  //   console.log("updateAccount")
  //   console.log(this.updatedAccount)
  //   this.accountService.updateAccount(this.updatedAccount).subscribe((res)=>{
  //     alert(res.response)

  //   })
  // }

  deleteAccount(){
    console.log("entra in delete account")
    this.accountService.deleteAccount(this.account).subscribe((res)=>{
      alert(res.response)

    })
  }



}
