import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Account } from 'src/Interfaces/account';
import { ManageAccountService } from 'src/Service/manage-account.service';
import { TokenStorageService } from 'src/Service/token-storage.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  account: Account={ email: "", name:"", password: "", surname: "", role:"", tel: "", id:""};
  isLoggedIn = false;
  isLoginFailed = false;
  errorMessage = '';
  roles: string[] = [];
  constructor(private accountService: ManageAccountService, private tokenStorage: TokenStorageService, private router:Router) { }
 
 
 
  ngOnInit(): void {
    if (this.tokenStorage.getToken()) {
      this.isLoggedIn = true;
      this.roles = this.tokenStorage.getUser().roles;
    }
  }

  onSubmit(): void {
        this.accountService.login(this.account).subscribe((res) =>{
        this.tokenStorage.saveToken(res.token);
        this.tokenStorage.saveUser(res.id);
        this.isLoginFailed = false;
        this.isLoggedIn = true;
        this.roles = this.tokenStorage.getUser().roles;
        this.router.navigate(['/']);
      }
    );
  }


}
