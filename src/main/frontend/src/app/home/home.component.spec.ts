import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { location } from 'src/Interfaces/location';

import { HomeComponent } from './home.component';

describe('HomeComponent', () => {
  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;

  beforeEach(async () => {
    TestBed.configureTestingModule({imports: [HttpClientTestingModule]});
    await TestBed.configureTestingModule({
      declarations: [ HomeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });


  // it('The map should have not null dimension',()=>{
  //   const compiled = fixture.nativeElement;
  //   console.log(compiled.querySelector('.agm-map').style.height);
  //   expect(compiled.querySelector('agm-map').style.height).toBe(500);
  // });

  
  it('should corectly setDirection', ()=>{
    let destionation:location={lat:0,lng:0};
    destionation.lat= 45.51686960789838;
    destionation.lng =  8.750322514022459;
    component.setDirection(destionation);
    expect(component.destination).toEqual(destionation);
  })


  // it('should corectly setCurrentLocation', ()=>{
  //   let currentLocation!:location;
  //   navigator.geolocation.getCurrentPosition(resp => {
  //     currentLocation.lat=resp.coords.latitude;
  //     currentLocation.lng=resp.coords.longitude});
  //     console.log("---------------------------------------")
  //     console.log(currentLocation)
  //    component.setCurrentPosition();
  //    expect(component.markers[0].location).toEqual(currentLocation);
  //  })



});
