import { MapsAPILoader, Marker } from '@agm/core';
import { Component, NgZone, OnInit } from '@angular/core';
import { imageGallery } from 'src/Interfaces/imageGallery';
import{location} from 'src/Interfaces/location';
import{marker} from 'src/Interfaces/marker';
import { GalleryService } from 'src/Service/gallery.service';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})

export class HomeComponent implements OnInit {
  
	//gooble maps' zoon
  	zoom = 14;
	//this mode permit also street adaptable for bicycle
  	travelMode:any="WALKING";
	//map-direction
  	origin!:location;
  	destination!:location;
  	markers!: marker[] ;
	//waypoints is helpfull for modify the itinerary
  	waypoints: any = [/* {location: { lat: 45.5319023, lng: 8.7291678}}*/];
	// collection of image for gallery
	imgCollection!: imageGallery[] ;
	

	constructor(public galleryService:GalleryService) { }

	ngOnInit(): void {
		//google map marker with client current position
		this.setCurrentPosition()
		//turistic marker initialization
		this.initializeMarkers()
		this.origin=this.markers[1].location;
		//gallery image initialization
		this.getAllGalleryImages()
		
	}


	/**
	 * This method is used for call Service galleryService and fetch a getAll to backend
	 * Adding the correct folder path 
	 */
	getAllGalleryImages(){
		this.galleryService.getAllGalleryImage().subscribe((res)=>{
			this.imgCollection=res;
			//add for each image fileName their own path 
			for(let i=0;i<this.imgCollection.length;i++){
				this.imgCollection[i].image="./assets/images/gallery/"+this.imgCollection[i].image;
				this.imgCollection[i].thumbImage=this.imgCollection[i].image;
		}
		});
	}


	/**
	 * Set the destination of direction agm-direction component  
	 *
	 */
	setDirection(location:location){
		this.destination={lat:location.lat,lng:location.lng}
	}



	/**
	 * Set the customers current position to the first merker object in markers array 
	 *
	 */
	setCurrentPosition(){
		navigator.geolocation.getCurrentPosition(resp => {
															this.markers[0].location.lat=resp.coords.latitude
															this.markers[0].location.lng=resp.coords.longitude})
	}



	/**
	 * Initialize all markers in the list of merker's object . 
	 * They are : Customer position , myRestaurant , Ponte tibetano , Belvedere di Tornavento , Mulino del pericolo
	 *
	 */
	initializeMarkers(){
		this.markers=[
			{
				location:{
				lat: 0,
				lng: 0,
				},
				label: "C",
				title:"Your Position",
				description:"That's your current position",
				url:""
			},
			{
				location:{
				lat: 45.51686960789838,
				lng: 8.750322514022459,
				},
				label: "0",
				title:"Ristorante san pietro",
					description:"description a",
					url:""
			},
			{
				location:{
					lat: 45.5274179,
					lng: 8.7184008,
				},
				label: "A",
				title:"Ponte tibetano",
				description:"description a",
				url:"https://i1.wp.com/walkaboutwanderer.com/wp-content/uploads/2017/03/P1090960.jpg?resize=900%2C660"
			},
			{
				location:{
					lat: 45.5831964,
					lng: 8.7067419,
				},
				label: "B",
				title:"Belvedere di Tornavento",
					description:"description b",
					url:"https://th.bing.com/th/id/R.a9aca0abb5c44e1082c8de1d7ab72a44?rik=5VJjKEKIk3cK8w&riu=http%3a%2f%2fparcoticino.eguide.it%2fwp-content%2fuploads%2f2014%2f03%2fBelvedere01.jpg&ehk=wLvRN2A4fZT1UCXsGrd4J%2bWqDK36o8RzOSpdW1J0CUA%3d&risl=&pid=ImgRaw&r=0&sres=1&sresct=1"
			},
			{
				location:{
					lat: 45.5191061,
					lng: 8.7241989,
				},
				label: "C",
				title:"Mulino del Pericolo",
					description:"description c ",
					url:"https://th.bing.com/th/id/OIP.Oj65uXgP67fB8ALFqv8MTAHaHa?pid=ImgDet&rs=1"
			}
			
		]
  }

}
