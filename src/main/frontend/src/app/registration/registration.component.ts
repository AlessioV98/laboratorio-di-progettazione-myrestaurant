import { Component, Input, OnInit } from '@angular/core';
import { Account } from 'src/Interfaces/account';
import { ManageAccountService } from 'src/Service/manage-account.service';
import { Router } from '@angular/router';
import { TokenStorageService } from 'src/Service/token-storage.service';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})

export class RegistrationComponent implements OnInit {
  account: Account={ email: "", name:"", password: "", surname: "", role:"USER", tel: "", id:""};
  isLoggedIn = false;
  isLoginFailed = false;
  errorMessage = '';
  roles: string[] = [];
  constructor(private accountService: ManageAccountService, private tokenStorage: TokenStorageService, private router:Router) { }
  ngOnInit(): void {
    if (this.tokenStorage.getToken()) {
      this.isLoggedIn = true;
      this.roles = this.tokenStorage.getUser().roles;
    }
  }

  onSubmit(): void {
        console.log(this.account)
        this.accountService.register(this.account).subscribe((res) =>{
        alert(res.response)
        this.router.navigate(['/']);
        //this.reloadPage();
      }
    );
  }
  reloadPage(): void {
    window.location.reload();
  }

}

