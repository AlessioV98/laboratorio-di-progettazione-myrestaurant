import { Component, OnInit } from '@angular/core';
import { Account } from 'src/Interfaces/account';
import { Reservation } from 'src/Interfaces/reservation';
import { ManageAccountService } from 'src/Service/manage-account.service';
import { ReservationService } from 'src/Service/reservation.service';
import { TokenStorageService } from 'src/Service/token-storage.service';

@Component({
  selector: 'app-manage-tables',
  templateUrl: './manage-tables.component.html',
  styleUrls: ['./manage-tables.component.css']
})
export class ManageTablesComponent implements OnInit {
  inEdit: boolean = false;
  launch!: Reservation[];
  dinner!: Reservation[];
  date!: string;
  account: Account={ email: "", name:"", password: "", surname: "", role:"", tel: "", id:""};
  inSearch: boolean = false;
  reservation: Reservation = {id:"",email: "", datetime: "", tel: "", people: 0, tablenumber: 0}

  constructor(public accountService:ManageAccountService, public reservationService:ReservationService, public tokenService:TokenStorageService) { }

  ngOnInit(): void {
  //   this.reservationService.getAllReservation().subscribe((reser)=>{
  //     this.reservations = reser.reservations
  // });
    //this.getAllAccount();
  //   this.formdata = new FormGroup({
  //     emailid: new FormControl("angular@gmail.com"),
  //     passwd: new FormControl("abcd1234")
  //  });
  }

  onSubmit(): void {
    this.reservationService.addTableToReservation(this.reservation.id, this.reservation.tablenumber).subscribe((res) =>{
      alert(res.response);
     });
    this.search(this.date)

  }
   edit(id: string, tablenumber:number) : void{
     
    this.inEdit = true;
    this.reservation.id = id;
    this.reservation.datetime = this.reservation.datetime;
    this.reservation.people = this.reservation.people;
    this.reservation.tel = this.account.tel;
    this.reservation.email = this.account.email;
    this.reservation.tablenumber = tablenumber;
    
  } 
  search(date:string) : void{
    this.date = date;
    this.inSearch = true;
    this.reservationService.getReservationByDay(date).subscribe((res) =>{
     this.launch = res.launch;
     this.dinner = res.dinner;
      
    });
  }

  delete(id : string) : void{
    this.reservationService.deleteReservation(id).subscribe((res) =>{
      alert("Reservation deleted");
      
    });
  } 
}
