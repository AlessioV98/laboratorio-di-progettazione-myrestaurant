import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';

import { ManageTablesComponent } from './manage-tables.component';

describe('ManageTablesComponent', () => {
  let component: ManageTablesComponent;
  let fixture: ComponentFixture<ManageTablesComponent>;

  beforeEach(async () => {
    TestBed.configureTestingModule({imports: [HttpClientTestingModule]});
    await TestBed.configureTestingModule({
      declarations: [ ManageTablesComponent ],
      imports: [ RouterTestingModule, FormsModule,]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageTablesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
