import { Component, OnInit } from '@angular/core';
import { Account } from 'src/Interfaces/account';
import { ManageAccountService } from 'src/Service/manage-account.service';
import { TokenStorageService } from 'src/Service/token-storage.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  isLoggedIn = false;
  account: Account={ email: "", name:"", password: "", surname: "", role:"", tel: "", id:""};
  constructor( private tokenStorageService: TokenStorageService, private accountService: ManageAccountService) { 
    this.isLoggedIn = !!this.tokenStorageService.getToken();
    if(this.isLoggedIn==true){
      accountService.getAccount(tokenStorageService.getUser()).subscribe((res) =>{
        this.account.role = res.role;
      });
    }
  }

  ngOnInit(): void {
  }
  logout(): void {
    this.tokenStorageService.signOut();
  }
}

