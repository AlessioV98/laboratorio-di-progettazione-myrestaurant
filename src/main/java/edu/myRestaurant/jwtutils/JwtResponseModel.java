package edu.myRestaurant.jwtutils;

import java.io.Serializable; 
public class JwtResponseModel implements Serializable {
   /**
   *
   */
   private static final long serialVersionUID = 1L;
   private final String token;
   private final String id;
   public JwtResponseModel(String token, String id) {
      this.token = token;
      this.id = id;
   }
   public String getToken() {
      return token;
   }
   public String getId() {
      return id;
   }
}

