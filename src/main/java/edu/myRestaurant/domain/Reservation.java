package edu.myRestaurant.domain;


import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;


import org.hibernate.annotations.GenericGenerator;



@Entity
@Table(name = "reservations")
public class Reservation {

	
	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

	@Column(name = "email")
    private String email;
    
	@Column(name = "datetime")
    private LocalDateTime datetime;

	@Column(name = "tel")
    private String tel;
    
    
	@Column(name = "people")
    private int people;

	@Column(name = "tablenumber")
    private int tablenumber;

    public Reservation(){}
    
	

	public Reservation(String email, String tel, LocalDateTime datetime, int people, int tablenumber) {
		super();
		this.email = email;
		this.tel = tel;
		this.datetime = datetime;
		this.people = people;
		this.tablenumber = tablenumber;
	}





	public int getTablenumber() {
		return tablenumber;
	}



	public void setTablenumber(int tablenumber) {
		this.tablenumber = tablenumber;
	}



	public String getId() {
		return id;
	}



	public void setId(String id) {
		this.id = id;
	}



	public String getEmail() {
		return email;
	}



	public void setEmail(String email) {
		this.email = email;
	}



	public LocalDateTime getDatetime() {
		return datetime;
	}



	public void setDatetime(LocalDateTime datetime) {
		this.datetime = datetime;
	}



	public String getTel() {
		return tel;
	}



	public void setTel(String tel) {
		this.tel = tel;
	}



	public int getPeople() {
		return people;
	}



	public void setPeople(int people) {
		this.people = people;
	}
	
	
}