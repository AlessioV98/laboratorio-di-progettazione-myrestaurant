package edu.myRestaurant.domain;


import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "menus")
public class Menu implements Serializable{
	
  @Id
  @GeneratedValue(generator = "uuid")
  @GenericGenerator(name = "uuid", strategy = "uuid2")
  private String id;

  private String title;

  
  private String description;

 
  private String fileName;


  @ManyToMany(cascade = {CascadeType.MERGE})
  private List<Dish> dishes;

  public Menu() {
  }

  



  public Menu(String title, String description, String fileName, List<Dish> dishes) {
    this.title = title;
    this.description = description;
    this.fileName = fileName;
    this.dishes = dishes;
  }



  public Menu(String id, String title, String description, String fileName, List<Dish> dishes) {
    this.id = id;
    this.title = title;
    this.description = description;
    this.fileName = fileName;
    this.dishes = dishes;
  }




  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getFileName() {
    return fileName;
  }

  public void setFileName(String fileName) {
    this.fileName = fileName;
  }

  public List<Dish> getDishes() {
    return dishes;
  }

  public void setDishes(List<Dish> dishes) {
    this.dishes = dishes;
  }

  public void addDish(Dish dish) {
		dishes.add(dish);
	}
	
	public void removeDish(Dish dish) {
		dishes.remove(dish);
	}



}