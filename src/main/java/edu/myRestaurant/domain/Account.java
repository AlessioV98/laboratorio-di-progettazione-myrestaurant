package edu.myRestaurant.domain;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;


@Entity
@Table(name = "accounts")
public class Account {

	
	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

	@Column(name = "email")
    private String email;

	@Column(name = "name")
    private String name;

	@Column(name = "surname")
    private String surname;
	
    @Column(name = "password")
    private String password;
    
	@Column(name = "tel")
    private String tel;
    
    @Column(name = "role")
    private String role;
    
    public Account(){}
    
	

	public Account(String email, String name, String surname, String tel, String password, String role) {
		super();
		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		this.email = email;
		this.name = name;
		this.surname = surname;
		this.tel = tel;
		setPassword(passwordEncoder.encode(password));
		System.out.println("the password encrypted is: "+ this.password);
		this.role = role;
	}

	public String getRole() {
		return role;
	}

	public String getTel() {
		return tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}


	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
}