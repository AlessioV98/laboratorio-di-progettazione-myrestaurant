package edu.myRestaurant.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "gallery")
public class ImageGallery {

    @Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "id", strategy = "uuid2")
    private String id;

    @Column(name = "image")
    private String image;

    public ImageGallery(String image){
        this.image=image;
    }

    public ImageGallery(){}

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public String getimage() {
        return image;
    }

    public void setimage(String image) {
        this.image = image;
    }

    
    
    
}
