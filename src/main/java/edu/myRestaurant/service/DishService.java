package edu.myRestaurant.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import edu.myRestaurant.domain.Dish;
import edu.myRestaurant.repository.DishRepository;

@Service
public class DishService {

	@Autowired
	private DishRepository dishRepository;

	public static final String UPLOAD_DIR_DISH = "src/main/frontend/src/assets/images/dish";

	public void save(Dish dish) {
		dishRepository.save(dish);
	}

	public List<Dish> getAllDishes() {
		List<Dish> dishes = new ArrayList<>();
		for (Dish d : dishRepository.findAll()) {
			dishes.add(d);
		}
		return dishes;
	}

	public Dish getDish(String id) {
		return dishRepository.findById(id)
				.orElseThrow(() -> new EntityNotFoundException());
	}

	public Dish getDishByName(String name) {
		return dishRepository.findByName(name);
	}

	public void updateDish(String id, Dish dish) {
		dishRepository.findById(id);
		dishRepository.save(dish);
	}

	public void deleteDish(String id) {
		dishRepository.deleteById(id);
	}

	public void deleteAllDishes() {
		dishRepository.deleteAll();
	}

	public boolean existsByName(String name) {
		return dishRepository.existsByName(name);
	}

	public boolean existsById(String id) {
		return dishRepository.existsById(id);
	}

	public void save(MultipartFile image, String name, String description, Float price) throws IOException {
		String fileName = image.getOriginalFilename();
		Dish dish = new Dish(name, description, price, fileName);
		UtilService.saveFile(UPLOAD_DIR_DISH, fileName, image);
		dishRepository.save(dish);
	}

	public void update(Dish dish, MultipartFile image) throws IOException {
		Dish dishToUpdate = dishRepository.getById(dish.getId());
		if (image != null) {
			UtilService.deleteFile(UPLOAD_DIR_DISH, dishToUpdate.getImageFileName());
			UtilService.saveFile(UPLOAD_DIR_DISH, image.getOriginalFilename(), image);
			dish.setImageFileName(image.getOriginalFilename());
		} else {
			dish.setImageFileName(dishToUpdate.getImageFileName());
		}
		dishRepository.save(dish);
	}

}
