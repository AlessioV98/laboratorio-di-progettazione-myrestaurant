package edu.myRestaurant.service;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import edu.myRestaurant.domain.Dish;
import edu.myRestaurant.domain.Menu;
import edu.myRestaurant.repository.DishRepository;
import edu.myRestaurant.repository.MenuRepository;

@Service
public class MenuService {

	@Autowired
	private  MenuRepository menuRepository;

	@Autowired
	private  DishRepository dishRepository;

	public static final String UPLOAD_DIR_MENU = "src/main/frontend/src/assets/images/menus";


	/**Used for saving menu object thanks the repository and saving the multipartfile 
	 * thanks to saveFile, the file is save into  ./src/main/resource/static/images/menu
	 * 
	 * @param file
	 * @param title
	 * @param description
	 * @return Menu
	 * @throws IOException
	 */
	public Menu save(MultipartFile file, String title, String description, List<Dish> dishes) throws IOException {
		String fileName = file.getOriginalFilename();
		Menu menu = new Menu(title, description, fileName, dishes);
		UtilService.saveFile(UPLOAD_DIR_MENU, fileName, file);
		return menuRepository.save(menu);
	}




	/**Used for saving menu object thanks the repository and saving the multipartfile 
	 * thanks to saveFile, the file is save into  ./src/main/resource/static/images/menu
	 * 
	 * @param file
	 * @param title
	 * @param description
	 * @return Menu
	 * @throws IOException
	 */

	public void update(Menu menu, MultipartFile image) throws IOException {
		Menu menuToUpdate = menuRepository.getById(menu.getId());
		if(image != null){
			UtilService.deleteFile(UPLOAD_DIR_MENU, menuToUpdate.getFileName());
			UtilService.saveFile(UPLOAD_DIR_MENU, image.getOriginalFilename(), image);
			menu.setFileName(image.getOriginalFilename());
		} else {
			menu.setFileName(menuToUpdate.getFileName());
		}
		menuRepository.save(menu);
	}


	
	/** 
	 * @return List<Menu>
	 */
	public List<Menu> getMenus() {
		List<Menu> menus =new ArrayList<>();
		for (Menu menu: menuRepository.findAll()) {
			menus.add(menu);
		}
		return menus;
	}


	public void removeAll() {
		menuRepository.deleteAll();
	}



	
	/** Used for deleting the Menu object present into DB and deleting the images associated
	 * @param id
	 */
	public void deleteMenu(String id) {
		Menu menu = getMenu(id);
		UtilService.deleteFile(UPLOAD_DIR_MENU, menu.getFileName());
		menuRepository.deleteById(id);
	}


	
	/** 
	 * @param id
	 * @return Menu
	 */
	public Boolean existMenuByTitle(String title) {
		return menuRepository.existsMenuByTitle(title);
	}

	/** 
	 * @param id
	 * @return Menu
	 */
	public Boolean existMenuById(String id) {
		return menuRepository.existsById(id);
	}

	/** 
	 * @param id
	 * @return Menu
	 */
	public Menu getMenu(String id) {
		return menuRepository.getById(id);
	}

    public void removeDish(String id) {
		List<Menu> menus = menuRepository.findMenuByDishesId(id);
		Dish dish = dishRepository.getById(id);
		for (Menu menu : menus) {
			menu.removeDish(dish);
			menuRepository.save(menu);
		}
    }



	public void removeDishes(List<Dish> dishes) {
		for (Dish dish : dishes){ 
			this.removeDish(dish.getId());
		}
	}
	
}