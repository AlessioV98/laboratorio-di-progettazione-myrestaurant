package edu.myRestaurant.service;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import edu.myRestaurant.domain.ImageGallery;
import edu.myRestaurant.repository.ImageGalleryRepository;

@Service
public class ImageGalleryService {

    @Autowired
    private ImageGalleryRepository imageGalleryRepository;

  

    public static final String UPLOAD_DIR_MENU = "src/main/frontend/src/assets/images/gallery/";


    /** 
     * This method call UtilService for store the Multipart file
     * and store with ImageGalleryRepository .save
     * @param file
     */
    public void save(MultipartFile file) throws IOException{
        String fileName = file.getOriginalFilename();
        UtilService.saveFile(UPLOAD_DIR_MENU,fileName , file);
        ImageGallery ImageGallery=new ImageGallery(fileName);
        imageGalleryRepository.save(ImageGallery);
    }

    /** 
     * This method return the ImageGallery object with is id
     * @param id
     */
    public ImageGallery getImageGalleryImage(String id){
        return imageGalleryRepository.getById(id);
    }

     /** 
     * This method return the ImageGallery object with is id
     * @param id
     */
    public boolean existImageGalleryById(String id){
        System.out.println(id);
        return imageGalleryRepository.existsById(id);
    }

     /** 
     * This method delete an image ImageGallery record and recall deleteFile of UtilService for deleting image
     * @param id
     */
    public void deleteById(String id){
        UtilService.deleteFile(UPLOAD_DIR_MENU, imageGalleryRepository.getById(id).getimage());
        imageGalleryRepository.deleteById(id);
    }

     /** 
     * This method return all ImageGallery images
     */
    public List<ImageGallery> getAll(){
        List<ImageGallery>ImageGallery=imageGalleryRepository.findAll();
        return ImageGallery;
    }  


}
