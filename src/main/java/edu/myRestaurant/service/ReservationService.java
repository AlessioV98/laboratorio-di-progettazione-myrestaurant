package edu.myRestaurant.service;


import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import edu.myRestaurant.domain.Account;
import edu.myRestaurant.domain.Reservation;
import edu.myRestaurant.jwtutils.TokenManager;
import edu.myRestaurant.repository.ReservationRepository;


@EnableScheduling
@Service
public class ReservationService  {

    @Autowired
    private ReservationRepository reservationRepository;

    @Autowired
    private EmailService emailService;

    @Autowired
    private AccountService accountService;

    @Autowired
    private TokenManager tokenManager;

    private static final String FORMATO_DATA = "yyyy-MM-dd'T'HH:mm";

    /** 
 	* This method saves the Reservation in ReservationRepository, calling ReservationRepository.save(Reservation res).
	* @param Reservation res
 	*/
    public void save(Reservation res){
        reservationRepository.save(res);
    }


    /** 
 	* This method updates the Reservation in ReservationRepository, calling ReservationRepository.setReservationById(String id, String email, String tel, Date datetime, int people);
	* @param String id	 
 	* @param String email
	* @param String tel
	* @param LocalDateTime datetime,
	* @param int people
 	*/
    public String update(String id, String email, String tel, LocalDateTime datetime, int people, int tablenumber){
        int capacity = 50;
        int launchPeople = 0;
        int dinnerPeople = 0;
        Reservation oldRes = getReservation(id);
        if(datetime.isBefore(LocalDateTime.now()))
          return "Insert a valid date";
        if(people <= 0)
          return "Number of people must be greater than 0";
        if((datetime.getHour() <= 10 || datetime.getHour() >= 15) && (datetime.getHour() <= 17 || datetime.getHour() >= 22))
          return "Sorry, we do not accept reservations for that hour";
        if(LocalDateTime.now().plusMinutes(30).isAfter(datetime))
          return "You must reserve with at least half hour in advance";
        if(datetime.getHour() < 16){
            if (getPeopleByDayLaunch(datetime.toLocalDate()) != null)
                launchPeople = getPeopleByDayLaunch(datetime.toLocalDate());
            else
                launchPeople = 0;
          }
        else{
            if(getPeopleByDayDinner(datetime.toLocalDate()) != null)
                dinnerPeople = getPeopleByDayDinner(datetime.toLocalDate());
            else
                dinnerPeople = 0;
          }
        if(datetime.getHour() < 16){
            if(launchPeople - oldRes.getPeople() + people <= capacity){
              reservationRepository.setReservationById(id, email, tel, datetime, people, tablenumber);
              return "ok";
            }
            else{
              return "Cannot create reservation at launch time because available seats are: "+(capacity-launchPeople);
            }
          }
          else{
            if(dinnerPeople - oldRes.getPeople() + people <= capacity){
              reservationRepository.setReservationById(id, email, tel, datetime, people, tablenumber);
              return "ok";
            }
            else{
              return "Cannot create reservation at dinner time because available seats are: "+(capacity-dinnerPeople);
            }
          }
    }

      /** 
 	* This method sends an email notification (at midnight) to every account email that has a valid reservation for that day
 	*/
    @Scheduled(cron = "0 0 0 * * ?")
    public void sendNotification() {
      List<Reservation> reservations = getAllReservations();
      for(int i=0; i<reservations.size();i++){
        Reservation res = reservations.get(i);
        if(res.getDatetime().toLocalDate().toString().equals(LocalDate.now().toString()))
          emailService.sendEmail(res.getEmail(), "[Reservation Remainder]", "Conferma di avvenuta prenotazione in data: "+res.getDatetime().toString().replace("T", " ")+", per "+res.getPeople()+" persone, con numero di telefono: "+res.getTel());
      }
    }


    /** 
 	* This method return true iff email exists in ReservationRepository, calling ReservationRepository.existsByEmail(String email).
	* @param String email
 	*/
    public boolean existsByEmail(String email){
        return reservationRepository.existsByEmail(email);
    }

    /** 
 	* This method returns the Reservation that matches that id, calling ReservationRepository.findByID(id).
 	* @param String id
 	*/
    public Reservation getReservation(String id){
        return reservationRepository.findById(id);
    }

    /** 
 	* This method returns a list of all Reservations, calling ReservationRepository.findAll().
 	*/
    public List<Reservation> getAllReservations(){
        return reservationRepository.findAll();
    }


    /** 
 	* This method deletes the Reservation that matches that id, calling ReservationRepository.deleteByID(id).
 	* @param String id
 	*/
    public void deleteReservation(String id) {
        reservationRepository.deleteById(id);
    }


    /** 
 	* This method deletes all Accounts, calling ReservationRepository.deleteAll().
 	*/
    public void deleteAllReservations() {
        reservationRepository.deleteAll();
    }


    /** 
 	* This method returns the Reservation that matches that email, calling ReservationRepository.findByEmail(String email).
 	* @param String email
 	*/
    public Reservation getReservationEmail(String email) {
        return reservationRepository.findByEmail(email);
    }

    /** 
 	* This method returns the number of people reserved that day for launch time, calling ReservationRepository.getPeopleByDay(LocalDate date).
 	* @param LocalDate date
 	*/

    public Integer getPeopleByDayLaunch(LocalDate date) {
        LocalDateTime start = LocalDateTime.of(date, LocalTime.parse("10:00"));
        LocalDateTime end = LocalDateTime.of(date, LocalTime.parse("15:00"));
        return reservationRepository.getPeopleByDay(start, end);
    }

    /** 
 	* This method returns the number of people reserved that day for dinner time, calling ReservationRepository.getPeopleByDay(LocalDate date).
 	* @param LocalDate date
 	*/

    public Integer getPeopleByDayDinner(LocalDate date) {
        LocalDateTime start = LocalDateTime.of(date, LocalTime.parse("18:00"));
        LocalDateTime end = LocalDateTime.of(date, LocalTime.parse("23:00"));
        return reservationRepository.getPeopleByDay(start, end);
    }

    /** 
 	* This method returns the reservations for that day, calling ReservationRepository.getReservationByDay(LocalDate date).
 	* @param LocalDate date
 	*/
    public List<Reservation> getReservationByDay(LocalDate date) {
        return reservationRepository.getReservationByDay(date);
    }


    public String createReservation(String datetime, int nPeople, String token) {
        
        Account account = accountService.getAccountEmail(tokenManager.getUsernameFromToken(token.substring(7)));
        Reservation res = new Reservation(account.getEmail(), account.getTel(), LocalDateTime.parse(datetime, DateTimeFormatter.ofPattern(FORMATO_DATA)), nPeople,0);
        int capacity = 50;
        int launchPeople = 0;
        int dinnerPeople = 0;
        if(res.getDatetime().isBefore(LocalDateTime.now()))
          return "Insert a valid date";
        if(res.getPeople() <= 0)
          return "Number of people must be greater than 0";
        if((res.getDatetime().getHour() <= 10 || res.getDatetime().getHour() >= 15) && (res.getDatetime().getHour() <= 17 || res.getDatetime().getHour() >= 22))
          return "Sorry, we do not accept reservations for that hour";
        if(LocalDateTime.now().plusMinutes(30).isAfter(res.getDatetime()))
          return "You must reserve with at least half hour in advance";
        if(res.getDatetime().getHour() < 16){
          if (getPeopleByDayLaunch(res.getDatetime().toLocalDate()) != null)
              launchPeople = getPeopleByDayLaunch(res.getDatetime().toLocalDate());
          else
              launchPeople = 0;
        }
        else{
          if(getPeopleByDayDinner(res.getDatetime().toLocalDate()) != null)
              dinnerPeople = getPeopleByDayDinner(res.getDatetime().toLocalDate());
          else
              dinnerPeople = 0;
        }
    
        
        if(res.getDatetime().getHour() < 16){
          if(launchPeople + res.getPeople() <= capacity){
            reservationRepository.save(res);
            return "ok";
          }
          else{
            return "Cannot create reservation at launch time because available seats are: "+(capacity-launchPeople);
          }
        }
        else{
          if(dinnerPeople + res.getPeople() <= capacity){
            reservationRepository.save(res);		
            return "ok";
          }
          else{
            return "Cannot create reservation at dinner time because available seats are: "+(capacity-dinnerPeople);
          }
        }
    }


    public List<Reservation> getReservationsByEmail(String email) {
      return reservationRepository.findAllByEmail(email);
    }
}
    


