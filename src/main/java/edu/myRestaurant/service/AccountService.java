package edu.myRestaurant.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.myRestaurant.domain.Account;
import edu.myRestaurant.repository.AccountRepository;


@Service
public class AccountService  {

    @Autowired
    private AccountRepository accountRepository;

    /** 
 	* This method saves the Account in AccountRepository, calling AccountRepository.save(Account account).
	* @param Account account
 	*/
    public void save(Account account){
        accountRepository.save(account);
    }


    /** 
 	* This method updates the Account in AccountRepository, calling AccountRepository.setAccountById(String id, String email, String name, String password, String surname, String role, String tel);
	* @param String id	 
 	* @param String email
	* @param String name
	* @param String password
	* @param String surname
	* @param String role
	* @param String tel
 	*/
    public void update(String id, String email, String name, String password, String surname, String role, String tel){
        accountRepository.setAccountById(id, email, name, password, surname, role, tel);
    }


    /** 
 	* This method return true iff email exists in AccountRepository, calling AccountRepository.existsByEmail(String email).
	* @param String email
 	*/
    public boolean existsByEmail(String email){
        return accountRepository.existsByEmail(email);
    }

    /** 
 	* This method returns the Account that matches that id, calling AccountRepository.findByID(id).
 	* @param String id
 	*/
    public Account getAccount(String id){
        return accountRepository.findById(id);
    }

    /** 
 	* This method returns a list of all Accounts, calling AccountRepository.findAll().
 	*/
    public List<Account> getAllAccounts(){
        return accountRepository.findAll();
    }


    /** 
 	* This method deletes the Account that matches that id, calling AccountRepository.deleteByID(id).
 	* @param String id
 	*/
    public void deleteAccount(String id) {
        accountRepository.deleteById(id);
    }


    /** 
 	* This method deletes all Accounts, calling AccountRepository.deleteAll().
 	*/
    public void deleteAllAccount() {
        accountRepository.deleteAll();
    }


    /** 
 	* This method returns the Account that matches that email, calling AccountRepository.findByEmail(String email).
 	* @param String email
 	*/
    public Account getAccountEmail(String email) {
        return accountRepository.findByEmail(email);
    }

    


}

