package edu.myRestaurant.service;

import java.nio.file.attribute.UserPrincipal;
import java.util.List;
import java.util.stream.Collectors;

import javax.transaction.Transactional;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import edu.myRestaurant.domain.Account;
import edu.myRestaurant.repository.AccountRepository;


@Service
public class LoginService implements UserDetailsService {

    @Autowired
    private AccountRepository accountRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    	//1. Load the user from the users table by username. If not found, throw UsernameNotFoundException.
    	final Account account = accountRepository.findByEmail(username);
        if (account == null) {
            throw new UsernameNotFoundException(username);
        }
        
        // 2. Convert/wrap the user to a UserDetails object and return it.
        UserDetails user =
                User.withUsername(account.getEmail())
                        .password(account.getPassword())
                        .roles(account.getRole())
                        .build();
        return user;
    }
}

