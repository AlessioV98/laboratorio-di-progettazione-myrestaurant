package edu.myRestaurant.service;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

import org.springframework.web.multipart.MultipartFile;

public class UtilService {



    /** Used for deleting a file given its path and its filename
	 * @param path
	 * @param fileName
	 */
	public static void deleteFile(String path, String fileName) {
		Path imagesPath = Paths.get(path + fileName);
		try {
			Files.delete(imagesPath);
			System.out.println("File "
					+ imagesPath.toAbsolutePath().toString()
					+ " successfully removed");
		} catch (IOException e) {
			System.err.println("Unable to delete "
					+ imagesPath.toAbsolutePath().toString()
					+ " due to...");
			e.printStackTrace();
		}
	}



	
	/** 
	 * This method permit to save a multipart file into static folder converted to InpuStream 
	 * and copied into file with the specific file path
	 * @param String uploadDir
	 * @param String fileName 
	 * @param multipartFile file
	 * @throws IOException
	 * 
	 */
	public static void saveFile(String uploadDir, String fileName,
        MultipartFile multipartFile) throws IOException {
        Path uploadPath = Paths.get(uploadDir);
         
        if (!Files.exists(uploadPath)) {
            Files.createDirectories(uploadPath);
        }
         
        try (InputStream inputStream = multipartFile.getInputStream()) {
            Path filePath = uploadPath.resolve(fileName);
            Files.copy(inputStream, filePath, StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException ioe) {        
            throw new IOException("Could not save image file: " + fileName, ioe);
        }      
    }

    
}
