package edu.myRestaurant.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter; 


import edu.myRestaurant.jwtutils.JwtAuthenticationEntryPoint;
import edu.myRestaurant.jwtutils.JwtFilter;
import edu.myRestaurant.service.AccountService;
import edu.myRestaurant.service.LoginService;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter{

	@Autowired
    LoginService loginService;

	@Autowired
	private JwtAuthenticationEntryPoint authenticationEntryPoint;

	@Autowired
   private JwtFilter filter;
	
   
	/** method is used to configure distinct security points for our 
	 * application (e.g. secure and non-secure urls, success handlers etc.). **/
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		
		http.cors().and().csrf().disable()
		.authorizeRequests()
		//necessary for bulding unique jar with angular pages
		.antMatchers("/static/**","/**").permitAll()
		.anyRequest().authenticated()
		.and()
		.exceptionHandling().authenticationEntryPoint(authenticationEntryPoint)
		.and()
		.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
		http.headers().frameOptions().disable();
      	http.addFilterBefore(filter, UsernamePasswordAuthenticationFilter.class);
	} 

	

	@Bean
	@Override
	public AuthenticationManager authenticationManagerBean() throws
	Exception {
	   return super.authenticationManagerBean();
	}
	
	
	/** method is used to configure distinct security points for our 
	 * application (resource static page and bootstrap libraries). **/
	// @Override
    // public void configure(WebSecurity web) {
    //     web.ignoring()
    //     .antMatchers("/images/*","/static/**", "/classes/**", "*/frontend");
    // }

	
	//  @Bean
	//     public DaoAuthenticationProvider authProvider() {
	//         DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
	//         authProvider.setUserDetailsService(loginService);
	//         authProvider.setPasswordEncoder(passwordEncoder());
	//         return authProvider;
	//  }

	//     @Override
	//     protected void configure(AuthenticationManagerBuilder auth) throws Exception {
	//         auth.authenticationProvider(authProvider());
	//     }
	    

	@Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

}