package edu.myRestaurant.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import edu.myRestaurant.domain.ImageGallery;
import edu.myRestaurant.service.ImageGalleryService;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
public class ImageGalleryController {
    
  @Autowired
  ImageGalleryService imageGalleryService;

  String message;




 
// =========================================== 	GET /gallery/getAllGallery ===============================


/** 
 * This method get all ImageGallery with fileName and id usefull for delete
 * 
 */
  @GetMapping("/gallery/getAllGallery")
  public ResponseEntity<?> getAccount() {
    List<ImageGallery> gallery= imageGalleryService.getAll();
    return new ResponseEntity<>(gallery, HttpStatus.OK);
  }


// =========================================== 	DELETE /gallery/deleteImageGallery/{id} ===============================


/** 
 * This method delete image with is id
 * @param id
 */
  @DeleteMapping("/gallery/deleteImageGallery/{id}")
  public ResponseEntity<?> deleteAccount(@PathVariable("id") String id) {

    Map<String, Object> jsonResponse = new HashMap<String, Object>();

    if (imageGalleryService.getImageGalleryImage(id)==null) {
      jsonResponse.put("response", "Image gallery not foud");
      return new ResponseEntity<>(jsonResponse, HttpStatus.NOT_FOUND);
    } else {
      imageGalleryService.deleteById(id);
      jsonResponse.put("response", "Image gallery deleted succesfully");
      return new ResponseEntity<>(jsonResponse, HttpStatus.OK);
    }
  }

  // =========================================== 	POST /gallery/createImageGallery ===============================


    /** 
     * This method create new image gallery record
     * @param file
     */
    @PostMapping("/gallery/createImageGallery")
    public ResponseEntity<?> createImageGallery(@RequestPart MultipartFile file) throws IOException {
      Map<String, Object> jsonResponse = new HashMap<String, Object>();
        if (file.isEmpty() ) {
          jsonResponse.put("response", "The file is needed");
          return new ResponseEntity<>(jsonResponse, HttpStatus.BAD_REQUEST);
        }
        imageGalleryService.save(file);
        jsonResponse.put("response", "Image gallery created");
        return new ResponseEntity<>(jsonResponse, HttpStatus.CREATED);
    }

}
