package edu.myRestaurant.controller;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import edu.myRestaurant.domain.Dish;
import edu.myRestaurant.service.DishService;
import edu.myRestaurant.service.MenuService;

@RestController
@RequestMapping("/dish")
public class DishController {

  @Autowired
  private DishService dishService;
  @Autowired
  private MenuService menuService;

  @PostMapping("/upload")
  public ResponseEntity<?> uploadDish(
      @RequestPart MultipartFile dishImage,
      @RequestPart Dish dish) {

    if (dishImage.isEmpty() || dish.getName() == null
        || dish.getDescription() == null || dish.getPrice() <= 0) {
      return new ResponseEntity<>("All fields are needed", HttpStatus.BAD_REQUEST);
    }
    if (dishService.existsByName(dish.getName())) {
      return new ResponseEntity<>("Dish already exists", HttpStatus.CONFLICT);
    }
    try {
      dishService.save(dishImage, dish.getName(),
          dish.getDescription(), dish.getPrice());
    } catch (IOException e) {
      e.printStackTrace();
    }
    return new ResponseEntity<>("Dish created", HttpStatus.CREATED);

  }

  @PatchMapping("/update")
  public ResponseEntity<?> updateDish(
      @RequestPart(value = "dishImage", required = false) MultipartFile dishImage,
      @RequestPart Dish dish) {

    if (!dishService.existsById(dish.getId())) {
      return new ResponseEntity<>("Dish doesn't exist", HttpStatus.NOT_FOUND);
    }
    try {
      dishService.update(dish, dishImage);
    } catch (IOException e) {
      e.printStackTrace();
    }
    return new ResponseEntity<>("Dish Updated", HttpStatus.OK);

  }

  @GetMapping("/get/{id}")
  public ResponseEntity<?> getDish(
      @PathVariable("id") String id) {
    Dish dish = dishService.getDish(id);
    if (dish == null) {
      return new ResponseEntity<>("Dish not found", HttpStatus.NOT_FOUND);
    }
    return new ResponseEntity<>(dish, HttpStatus.OK);
  }

  @GetMapping("/getAll")
  public ResponseEntity<?> getAllDishes() {
    List<Dish> dishes = dishService.getAllDishes();
    return new ResponseEntity<>(dishes, HttpStatus.OK);
  }

  @DeleteMapping("/delete/{id}")
  public ResponseEntity<?> deleteDish(@PathVariable("id") String id) {
    if (dishService.getDish(id) == null) {
      return new ResponseEntity<>("Dish not found", HttpStatus.NOT_FOUND);
    } else {
      menuService.removeDish(id);
      dishService.deleteDish(id);
      return new ResponseEntity<>("Dish deleted", HttpStatus.OK);
    }
  }

  @DeleteMapping("/deleteAll")
  public ResponseEntity<?> deleteAll() {
    List<Dish> dishes = dishService.getAllDishes();
    menuService.removeDishes(dishes);
    dishService.deleteAllDishes();
    return new ResponseEntity<>("All Dishes deleted", HttpStatus.OK);
  }

}
