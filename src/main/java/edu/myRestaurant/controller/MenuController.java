package edu.myRestaurant.controller;

import java.io.IOException;
import java.util.List;

import javax.websocket.server.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import edu.myRestaurant.domain.Dish;
import edu.myRestaurant.domain.Menu;
import edu.myRestaurant.service.MenuService;


@RequestMapping("/menu")
@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class MenuController {

  @Autowired
  MenuService menuService;

  String message;

  // =========================================== POST /manageMenu/upload
  // ===============================
  // @PreAuthorize("hasRole('ADMIN')")
  @PostMapping("/upload")
  public ResponseEntity<?> uploadMenu(
      @RequestPart MultipartFile menuImage,
      @RequestPart Menu menu) {

    if (menuImage.isEmpty() || menu.getTitle() == null
        || menu.getDescription() == null || menu.getDishes().isEmpty()) {
      return new ResponseEntity<>("All fields are needed", HttpStatus.BAD_REQUEST);
    }
    if (menuService.existMenuByTitle(menu.getTitle())) {
      return new ResponseEntity<>("Menu already exists", HttpStatus.CONFLICT);
    }
    try {
      menuService.save(menuImage, menu.getTitle(),
          menu.getDescription(), menu.getDishes());
    } catch (IOException e) {
      e.printStackTrace();
    }
    return new ResponseEntity<>("Menu created", HttpStatus.CREATED);
  }

  // =========================================== PATCH /manageMenu/update
  // ===============================

  @PatchMapping("/update")
  public ResponseEntity<?> updateMenu(
    @RequestPart(value = "menuImage", required = false) MultipartFile menuImage,
    @RequestPart Menu menu) {

    if (!menuService.existMenuById(menu.getId())) {
      return new ResponseEntity<>("Menu doesn't exist", HttpStatus.NOT_FOUND);
    }
    try {
      menuService.update(menu, menuImage);
    } catch (IOException e) {
      e.printStackTrace();
    }
    return new ResponseEntity<>("Menu Updated", HttpStatus.OK);


  }

  // =========================================== DELETE /manageMenu/delete
  // ===============================

  @DeleteMapping("/delete/{id}")
  public ResponseEntity<?> deleteMenu(@PathVariable("id") String id) {
    if (menuService.getMenu(id) == null) {
      return new ResponseEntity<>("Menu not found", HttpStatus.NOT_FOUND);
    } else {
      menuService.deleteMenu(id);
      return new ResponseEntity<>("Menu deleted", HttpStatus.OK);
    }

  }

  // =========================================== DELETE /manageMenu/deleteAll
  // ===============================

  @DeleteMapping("/deleteAll")
  public ResponseEntity<?> deleteAll() {
    menuService.removeAll();
    return new ResponseEntity<>("All menus deleted", HttpStatus.OK);
  }

  // =========================================== GET /manageMenu/getMenu
  // ===============================

  @GetMapping("/get/{id}")
  public ResponseEntity<?> getMenu(
      @PathVariable("id") String id) {
    Menu menu = menuService.getMenu(id);
    if (menu == null) {
      return new ResponseEntity<>("Menu not found", HttpStatus.NOT_FOUND);
    }
    return new ResponseEntity<>(menu, HttpStatus.OK);
  }

  // =========================================== GET /manageMenu/getAllMenu
  // ===============================

  @GetMapping("/getAll")
  public ResponseEntity<?> getAllMenu() {
    List<Menu> menus = menuService.getMenus();
    return new ResponseEntity<>(menus, HttpStatus.OK);

  }

}
