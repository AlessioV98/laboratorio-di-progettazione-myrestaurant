package edu.myRestaurant.controller;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.websocket.server.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;

import edu.myRestaurant.domain.Account;
import edu.myRestaurant.domain.Reservation;
import edu.myRestaurant.jwtutils.JwtRequestModel;
import edu.myRestaurant.jwtutils.JwtResponseModel;
import edu.myRestaurant.jwtutils.TokenManager;
import edu.myRestaurant.service.ReservationService;
import edu.myRestaurant.service.AccountService;


@CrossOrigin(origins = "http://localhost:4200")
@RestController
public class ReservationController {

  @Autowired
  ReservationService reservationService;

  @Autowired
  AccountService accountService;

  @Autowired
  TokenManager tokenManager;

  String message;
  
  private ObjectMapper objectMapper = new ObjectMapper();




// =========================================== 	POST manageReservation/createReservation ===============================

/** 
 * This method handles a post http request on "/manageReservation/createReservation" API, creates a new Reservation (iff restaurant capacity is respected), saves it in ReservationRepository.
 * @param HttpServletRequest request
 * @param HttpServletResponse response
 * @param Map<String,Object> params
 */


@PostMapping("/manageReservation/createReservation")
public ResponseEntity<?> createReservation(HttpServletRequest request, HttpServletResponse response,
                                    @RequestBody Map<String, Object> params) throws JsonMappingException, JsonProcessingException {
    
    Map<String, Object> jsonResponse = new HashMap<String, Object>();
    HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.APPLICATION_JSON);
    String status = reservationService.createReservation(params.get("datetime").toString(), (int)params.get("people"), request.getHeader("Authorization"));
    if(status.equals("ok")){
      jsonResponse.put("response", "Reservation created");
      return new ResponseEntity<>(jsonResponse, headers, HttpStatus.CREATED);
    }
    else{
      jsonResponse.put("response", status);
      return new ResponseEntity<>(jsonResponse, headers, HttpStatus.ACCEPTED);
    }
}







// =========================================== 	GET manageReservation/getReservation ===============================


/** 
 * This method handles a get http request on "/manageReservation/getReservation/{id}" API, it returns the Reservation that matches that id.
 * @param HttpServletRequest request
 * @param HttpServletResponse response
 * @param String id
 */

@GetMapping("/manageReservation/getReservation/{id}")
public ResponseEntity<?> getReservation(HttpServletRequest request, HttpServletResponse response,
                                   @PathVariable("id") String id,
                                   @RequestHeader (name="Authorization") String token) throws JsonMappingException, JsonProcessingException {
//create json response	
System.out.println(id);
Map<String, Object> jsonResponse = new HashMap<String, Object>();
HttpHeaders headers = new HttpHeaders();
headers.setContentType(MediaType.APPLICATION_JSON);

Reservation reservation = reservationService.getReservation(id);
if(reservation==null){
  jsonResponse.put("response", "Reservation not found");
  return new ResponseEntity<>(jsonResponse, headers, HttpStatus.NOT_FOUND);
}
jsonResponse.put("response", "Reservation found");
jsonResponse.put("reservation", reservation);
return new ResponseEntity<>(jsonResponse, headers, HttpStatus.OK);
}


// =========================================== 	GET manageReservation/getReservation ===============================


/** 
 * This method handles a get http request on "/manageReservation/getReservationsByEmail/{email}" API, it returns the Reservations that match that email.
 * @param HttpServletRequest request
 * @param HttpServletResponse response
 * @param String email
 */

@GetMapping("/manageReservation/getAllReservationsByEmail/{email}")
public ResponseEntity<?> getAllReservationsByEmail(HttpServletRequest request, HttpServletResponse response,
                                   @PathVariable("email") String email,
                                   @RequestHeader (name="Authorization") String token) throws JsonMappingException, JsonProcessingException {

Map<String, Object> jsonResponse = new HashMap<String, Object>();
HttpHeaders headers = new HttpHeaders();
headers.setContentType(MediaType.APPLICATION_JSON);

List<Reservation> reservations = reservationService.getReservationsByEmail(email);
if(reservations==null){
  jsonResponse.put("response", "Reservations not found");
  return new ResponseEntity<>(jsonResponse, headers, HttpStatus.NOT_FOUND);
}
jsonResponse.put("response", "Reservations found");
jsonResponse.put("reservations", reservations);
return new ResponseEntity<>(jsonResponse, headers, HttpStatus.OK);
}

// @GetMapping("/manageAccount/getAccountUserName/{userName}")
// public ResponseEntity<?> getAccountUserName(HttpServletRequest request, HttpServletResponse response,
//                                         @PathVariable("userName") String userName) throws JsonMappingException, JsonProcessingException {
// //create json response	
// System.out.println(userName);
// Map<String, Object> jsonResponse = new HashMap<String, Object>();
// HttpHeaders headers = new HttpHeaders();
// headers.setContentType(MediaType.APPLICATION_JSON);

// Account account=accountService.getAccountUserName(userName);
// if(account==null){
//   jsonResponse.put("response", "Account not found");
//   return new ResponseEntity<>(jsonResponse, headers, HttpStatus.NOT_FOUND);
// }
// jsonResponse.put("response", "Account found");
// jsonResponse.put("account", account);
// return new ResponseEntity<>(jsonResponse, headers, HttpStatus.OK);
// }

// =========================================== 	GET manageReservation/getAllReservation ===============================


/** 
 * This method handles a get http request on "/manageReservation/getAllReservation" API, it returns every Reservation.
 * @param HttpServletRequest request
 * @param HttpServletResponse response
 */

@GetMapping("/manageReservation/getAllReservation")
public ResponseEntity<?> getAllReservation(HttpServletRequest request, HttpServletResponse response) throws JsonMappingException, JsonProcessingException {
//create json response	
Map<String, Object> jsonResponse = new HashMap<String, Object>();
HttpHeaders headers = new HttpHeaders();
headers.setContentType(MediaType.APPLICATION_JSON);

List<Reservation> reservations = reservationService.getAllReservations();

jsonResponse.put("response", "Reservation found");
jsonResponse.put("reservations", reservations);
return new ResponseEntity<>(jsonResponse, headers, HttpStatus.OK);
}


// =========================================== 	DELETE /manageReservation/deleteReservation ===============================

/** 
 * This method handles a delete http request on "/manageReservation/deleteReservation/{id}" API, it deletes the Reservation that matches that id.
 * @param HttpServletRequest request
 * @param HttpServletResponse response
 * @param String id
 */

@DeleteMapping("/manageReservation/deleteReservation/{id}")
public  ResponseEntity<?> deleteReservation(HttpServletRequest request, HttpServletResponse response,
                                  @PathVariable("id") String id,
                                  @RequestHeader (name="Authorization") String token) {
 
  //create json response	
  Map<String, Object> jsonResponse = new HashMap<String, Object>();
  HttpHeaders headers = new HttpHeaders();
  headers.setContentType(MediaType.APPLICATION_JSON);

  if(reservationService.getReservation(id)==null){
    jsonResponse.put("response", "Reservation not found");
    return new ResponseEntity<>(jsonResponse, headers, HttpStatus.NOT_FOUND);
  }else{
    reservationService.deleteReservation(id);
    jsonResponse.put("response", "deleted");
    return new ResponseEntity<>(jsonResponse, headers, HttpStatus.OK);
  }                
}



// =========================================== 	DELETE /manageReservation/deleteAllReservation ===============================

/** 
 * This method handles a delete http request on "/manageReservation/deleteAllReservation" API, it deletes every Reservation.
 * @param HttpServletRequest request
 * @param HttpServletResponse response
 */

@DeleteMapping("/manageReservation/deleteAllReservation")
public  ResponseEntity<?> deleteAllReservation(HttpServletRequest request, HttpServletResponse response ){
 
  //create json response	
  Map<String, Object> jsonResponse = new HashMap<String, Object>();
  HttpHeaders headers = new HttpHeaders();
  headers.setContentType(MediaType.APPLICATION_JSON);

    reservationService.deleteAllReservations();
    jsonResponse.put("response", "deleted all");
    return new ResponseEntity<>(jsonResponse, headers, HttpStatus.OK);
               
}




// =========================================== 	PATCH /manageReservation/updateReservation ===============================

/** 
 * This method handles a patch http request on "/manageReservation/updateReservation/{id}" API, it updates the Reservation that matches that id.
 * @param HttpServletRequest request
 * @param HttpServletResponse response
 * @param String id	 
 * @param String reservationJson
 */


@PatchMapping("/manageReservation/updateReservation/{id}")
public  ResponseEntity<?> updateReservation(HttpServletRequest request, HttpServletResponse response,
                                    @PathVariable("id") String id,
                                    @RequestBody Reservation res,
                                    @RequestHeader (name="Authorization") String token) throws IOException {
    //create json response
    Map<String, Object> jsonResponse = new HashMap<String, Object>();
    //create http headers
    HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.APPLICATION_JSON);
    
    // if (userName==null || password==null || role==null) {
    // 	jsonResponse.put("response", "All field are needed");
    // 	return new ResponseEntity<>(jsonResponse, headers, HttpStatus.BAD_REQUEST);
    // }
    Reservation reservation = reservationService.getReservation(id);
    if(reservation==null){
        jsonResponse.put("response", "Reservation doesn't exist");
        return new ResponseEntity<>(jsonResponse, headers, HttpStatus.NOT_FOUND);
    }
    String status = reservationService.update(id, res.getEmail(), res.getTel(), res.getDatetime(), res.getPeople(), res.getTablenumber());
    if(status.equals("ok")){
      jsonResponse.put("response", "Reservation updated");
      return new ResponseEntity<>(jsonResponse, headers, HttpStatus.CREATED);
    }
    else{
      jsonResponse.put("response", status);
      return new ResponseEntity<>(jsonResponse, headers, HttpStatus.ACCEPTED);
    }

}

// =========================================== 	GET manageReservation/getPeopleByDay ===============================


/** 
 * This method handles a get http request on "/manageReservation/getPeopleByDay" API, it returns the number of people reserved for that day.
 * @param HttpServletRequest request
 * @param HttpServletResponse response
 * @param String datetime
 */

@GetMapping("/manageReservation/getPeopleByDay")
public ResponseEntity<?> getPeopleByDay(HttpServletRequest request, HttpServletResponse response,
                            @RequestParam("datetime") String datetime) throws JsonMappingException, JsonProcessingException {
//create json response	
    int launchPeople = 0;
    int dinnerPeople = 0;

    ObjectMapper mapper = new ObjectMapper();
    mapper.registerModule(new JavaTimeModule());
    mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);


    LocalDate date = LocalDate.parse(datetime);
    Map<String, Object> jsonResponse = new HashMap<String, Object>();
    HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.APPLICATION_JSON);
    launchPeople = reservationService.getPeopleByDayLaunch(date);
    dinnerPeople = reservationService.getPeopleByDayDinner(date);
    

    jsonResponse.put("launch", launchPeople);
    jsonResponse.put("dinner", dinnerPeople);
    return new ResponseEntity<>(jsonResponse, headers, HttpStatus.OK);
}

// =========================================== 	GET manageReservation/getReservationByDay ===============================


/** 
 * This method handles a get http request on "/manageReservation/getReservationByDay" API, it returns every reservation for that day.
 * @param HttpServletRequest request
 * @param HttpServletResponse response
 * @param String datetime
 */


@GetMapping("/manageReservation/getReservationByDay/{date}")
public ResponseEntity<?> getReservationByDay(HttpServletRequest request, HttpServletResponse response,
                            @PathVariable("date") String date) throws JsonMappingException, JsonProcessingException {
//create json response	
    List<Reservation> launch = new ArrayList<Reservation>();
    List<Reservation> dinner = new ArrayList<Reservation>();

    ObjectMapper mapper = new ObjectMapper();
    mapper.registerModule(new JavaTimeModule());
    mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);

    LocalDate localDate = LocalDate.parse(date);
    Map<String, Object> jsonResponse = new HashMap<String, Object>();
    HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.APPLICATION_JSON);

    List<Reservation> reservations = reservationService.getReservationByDay(localDate);
    for(int i=0;i<reservations.size();i++){
      if(reservations.get(i).getDatetime().getHour()>16)
        dinner.add(reservations.get(i));
      else
        launch.add(reservations.get(i));

    }
    jsonResponse.put("launch", launch);
    jsonResponse.put("dinner", dinner);
    return new ResponseEntity<>(jsonResponse, headers, HttpStatus.OK);
}
// =========================================== 	POST manageReservation/addTabletoReservation ===============================

/** 
 * This method handles a post http request on "/manageReservation/addTabletoReservation" API, add a table to a reservation, updates it in ReservationRepository.
 * @param HttpServletRequest request
 * @param HttpServletResponse response
 * @param Map<String,Object> params
 */


@PostMapping("/manageReservation/addTableToReservation")
public ResponseEntity<?> addTableToReservation(HttpServletRequest request, HttpServletResponse response,
                                    @RequestBody Map<String, Object> params) throws JsonMappingException, JsonProcessingException {
    List<Reservation> reservations;
    List<Reservation> launch = new ArrayList<Reservation>();
    List<Reservation> dinner = new ArrayList<Reservation>();
    String status = "";
    Map<String, Object> jsonResponse = new HashMap<String, Object>();
    HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.APPLICATION_JSON);
    Reservation res = reservationService.getReservation(params.get("id").toString());
    reservations = reservationService.getReservationByDay(res.getDatetime().toLocalDate()); 
    for(int i=0;i<reservations.size();i++){
      if(reservations.get(i).getDatetime().getHour()>16)
         if(reservations.get(i).getTablenumber() == (int)params.get("tablenumber")){
          jsonResponse.put("response", "Table already assigned");
          return new ResponseEntity<>(jsonResponse, headers, HttpStatus.ACCEPTED);
         }
      else
         if(reservations.get(i).getTablenumber() == (int)params.get("tablenumber")){
           jsonResponse.put("response", "Table already assigned");
           return new ResponseEntity<>(jsonResponse, headers, HttpStatus.ACCEPTED);
         }
    }      

    status = reservationService.update(res.getId(), res.getEmail(), res.getTel(), res.getDatetime(), res.getPeople(), (int) params.get("tablenumber"));
    if(status.equals("ok")){
      jsonResponse.put("response", "Table added");
      return new ResponseEntity<>(jsonResponse, headers, HttpStatus.CREATED);
    }
    else{
      jsonResponse.put("response", status);
      return new ResponseEntity<>(jsonResponse, headers, HttpStatus.ACCEPTED);
    }
}
}