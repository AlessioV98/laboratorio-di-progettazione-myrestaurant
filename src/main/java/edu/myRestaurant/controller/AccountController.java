package edu.myRestaurant.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import edu.myRestaurant.domain.Account;
import edu.myRestaurant.jwtutils.JwtRequestModel;
import edu.myRestaurant.jwtutils.JwtResponseModel;
import edu.myRestaurant.jwtutils.TokenManager;
import edu.myRestaurant.service.AccountService;
import edu.myRestaurant.service.LoginService;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
public class AccountController {
  @Autowired
  private LoginService userDetailsService;
  @Autowired
  private AuthenticationManager authenticationManager;
  @Autowired
  private TokenManager tokenManager;
  @Autowired
  AccountService accountService;

  String message;




  // =========================================== POST login
  // ===============================

  
 /**
   * This method handles a post http request on "/login" API and it generates a
   * unique token.
   * 
   * @param JwtRequestModel request
   */
  @PostMapping("/login")
  public ResponseEntity<?> createToken(@RequestBody JwtRequestModel request) throws Exception {
    try {
      authenticationManager
          .authenticate(new UsernamePasswordAuthenticationToken(request.getEmail(), request.getPassword()));
    } catch (DisabledException e) {
      throw new Exception("USER_DISABLED", e);
    } catch (BadCredentialsException e) {
      throw new Exception("INVALID_CREDENTIALS", e);
    }
    final UserDetails userDetails = userDetailsService.loadUserByUsername(request.getEmail());
    final String jwtToken = tokenManager.generateJwtToken(userDetails);
    Account account = accountService.getAccountEmail(request.getEmail());
    return ResponseEntity.ok(new JwtResponseModel(jwtToken, account.getId()));
  }


// =========================================== POST manageAccount/createAccount
// ===============================


  /**
   * This method handles a post http request on "/manageAccount/createAccount"
   * API, creates a new Account, saves it in AccountRepository.
   * 
   * @param Account accountRequest
   */
  @PostMapping("/register")
  public ResponseEntity<?> createAccount(@RequestBody Account accountRequest) {
    Map<String, Object> jsonResponse = new HashMap<String, Object>();
    // BAD_REQUEST
    if (accountRequest.getEmail() == null || accountRequest.getPassword() == null) {
      jsonResponse.put("response", "All field are nedded");
      return new ResponseEntity<>(jsonResponse, HttpStatus.BAD_REQUEST);
    }
    // we create a new account with contructor for encode the password field
    Account account = new Account(accountRequest.getEmail(), accountRequest.getName(), accountRequest.getSurname(),
        accountRequest.getTel(), accountRequest.getPassword(), "USER");
    // Check if email already exist
    if (accountService.existsByEmail(account.getEmail())) {
      jsonResponse.put("response", "Account already exist");
      return new ResponseEntity<>(jsonResponse, HttpStatus.CONFLICT);
    } else {
      // service for saving account to repository
      account.setRole("USER");
      accountService.save(account);
      jsonResponse.put("response", "Account create");
      return new ResponseEntity<>(account, HttpStatus.CREATED);
    }
  }

  // =========================================== GET manageAccount/getAccount
  // ===============================

  /**
   * This method handles a get http request on "/manageAccount/getAccount/{id}"
   * API, it returns the Account that matches that id.
   * 
   * @param String id
   */

  @GetMapping("/manageAccount/getAccount/{id}")
  public ResponseEntity<?> getAccount(@PathVariable("id") String id) {
    Account account = accountService.getAccount(id);
    if (account == null) {
      return new ResponseEntity<>("Account not found", HttpStatus.NOT_FOUND);
    }
    return new ResponseEntity<>(account, HttpStatus.OK);
  }

  // =========================================== GET manageAccount/getAllAccount
  // ===============================

  /**
   * This method handles a get http request on "/manageAccount/getAllAccount" API,
   * it returns every Account.
   */

  @GetMapping("/manageAccount/getAllAccount")
  public ResponseEntity<?> getAllAccount() {
    List<Account> accounts = accountService.getAllAccounts();
    return new ResponseEntity<>(accounts, HttpStatus.OK);
  }

  // =========================================== DELETE /manageAccount/delete
  // ===============================

  /**
   * This method handles a delete http request on
   * "/manageAccount/deleteAccount/{id}" API, it deletes the Account that matches
   * that id.
   * 
   * @param String id
   */
  @DeleteMapping("/manageAccount/deleteAccount/{id}")
  public ResponseEntity<?> deleteAccount(@PathVariable("id") String id) {
    if (accountService.getAccount(id) == null) {
      return new ResponseEntity<>("Account not found", HttpStatus.NOT_FOUND);
    } else {
      accountService.deleteAccount(id);
      return new ResponseEntity<>("Account deleted succesfully", HttpStatus.OK);
    }
  }




// =========================================== 	DELETE /manageAccount/deleteAllAccount ===============================

/** 
   * This method handles a delete http request on
   * "/manageAccount/deleteAllAccount" API, it deletes every Account.
   */
  
  @DeleteMapping("/manageAccount/deleteAllAccount")
  public ResponseEntity<?> deleteAllAccount() {
    accountService.deleteAllAccount();
    return new ResponseEntity<>("All account deleted", HttpStatus.OK);
  }


  // =========================================== PATCH
  // /manageAccount/updateAccount ===============================

  /**
   * This method handles a patch http request on
   * "/manageAccount/updateAccount/{id}" API, it updates the Account that matches
   * that id.
   * 
   * @param String id
   * @param String email
   * @param String name
   * @param String password
   * @param String surname
   * @param String role
   * @param String tel
   */

  @PatchMapping("/manageAccount/updateAccount/{id}")
  public ResponseEntity<?> updateAccount(@PathVariable("id") String id , @RequestBody Account accountRequest) throws IOException {
    Account account = accountService.getAccount(id);
    if (account == null) {
      return new ResponseEntity<>("Account not found", HttpStatus.NOT_FOUND);
    }
    if (accountRequest.getEmail() == null || accountRequest.getName() == null || accountRequest.getPassword() == null || 
    accountRequest.getSurname() == null || accountRequest.getRole() == null || accountRequest.getTel() == null) {
      return new ResponseEntity<>("All fields are needed", HttpStatus.BAD_REQUEST);
    }
    accountService.update(id, accountRequest.getEmail(), accountRequest.getName(), accountRequest.getPassword(),
    accountRequest.getSurname(), accountRequest.getRole(), accountRequest.getTel());
    return new ResponseEntity<>(accountRequest, HttpStatus.OK);
  }
}