package edu.myRestaurant.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import edu.myRestaurant.service.AccountService;
import edu.myRestaurant.service.MenuService;
import com.fasterxml.jackson.databind.ObjectMapper;




@Controller
public class IndexController {
	
	
	@Autowired
    MenuService menuService;

	@Autowired
	AccountService accountService;

	private ObjectMapper objectMapper = new ObjectMapper();
	









	
// // =========================================== 	POST manageAccount/createAccount ===============================


// 	@PostMapping("/manageAccount/createAccount")
// 	public ResponseEntity<?> createAccount(HttpServletRequest request, HttpServletResponse response,
// 										@RequestBody String accountJson) throws JsonMappingException, JsonProcessingException {
// 		System.out.println(accountJson);
// 		// converto il json in entrata in un oggetto Account
// 		Account aus = objectMapper.readValue(accountJson, Account.class);
// 		//create json response	
// 		Map<String, Object> jsonResponse = new HashMap<String, Object>();
// 		//create http headers
// 		HttpHeaders headers = new HttpHeaders();
// 		headers.setContentType(MediaType.APPLICATION_JSON);

		
// 		// BAD_REQUEST
// 		if(aus.getUserName()==null || aus.getPassword()==null){
// 			jsonResponse.put("response", "All field are needed");

// 			return new ResponseEntity<>(jsonResponse, headers, HttpStatus.BAD_REQUEST);
// 		}
		
// 		//we create a new account with contructor for encode the password field
// 		Account account=new Account(aus.getUserName(), aus.getPassword(), aus.getRole());
// 		//CONFLICT E CREATED
// 		if(accountService.existsByUserName(account.getUserName())){	
// 			jsonResponse.put("response", "Account already exist");

// 			return new ResponseEntity<>(jsonResponse, headers, HttpStatus.CONFLICT);
// 		}else{
// 			//save to repository													
// 			accountService.save(account);		
// 			jsonResponse.put("response", "Account create");

// 			return new ResponseEntity<>(jsonResponse, headers, HttpStatus.CREATED);
// 		}
// 	}







// 	// =========================================== 	GET manageAccount/getAccount ===============================


// 	@GetMapping("/manageAccount/getAccount/{id}")
// 	public ResponseEntity<?> getAccount(HttpServletRequest request, HttpServletResponse response,
//                                        @PathVariable("id") String id) throws JsonMappingException, JsonProcessingException {
// 	//create json response	
//     System.out.println(id);
//     Map<String, Object> jsonResponse = new HashMap<String, Object>();
//     HttpHeaders headers = new HttpHeaders();
//     headers.setContentType(MediaType.APPLICATION_JSON);

//     Account account=accountService.getAccount(id);
//     if(account==null){
//       jsonResponse.put("response", "Account not found");
//       return new ResponseEntity<>(jsonResponse, headers, HttpStatus.NOT_FOUND);
//     }
//     jsonResponse.put("response", "Account found");
//     jsonResponse.put("menu", account);
//     return new ResponseEntity<>(jsonResponse, headers, HttpStatus.OK);
//   }
	


//   @GetMapping("/manageAccount/getAccountUserName/{userName}")
// 	public ResponseEntity<?> getAccountUserName(HttpServletRequest request, HttpServletResponse response,
//                                             @PathVariable("userName") String userName) throws JsonMappingException, JsonProcessingException {
// 	//create json response	
//     System.out.println(userName);
//     Map<String, Object> jsonResponse = new HashMap<String, Object>();
//     HttpHeaders headers = new HttpHeaders();
//     headers.setContentType(MediaType.APPLICATION_JSON);

//     Account account=accountService.getAccountUserName(userName);
//     if(account==null){
//       jsonResponse.put("response", "Account not found");
//       return new ResponseEntity<>(jsonResponse, headers, HttpStatus.NOT_FOUND);
//     }
//     jsonResponse.put("response", "Account found");
//     jsonResponse.put("menu", account);
//     return new ResponseEntity<>(jsonResponse, headers, HttpStatus.OK);
//   }

//   // =========================================== 	GET manageAccount/getAllAccount ===============================


// 	@GetMapping("/manageAccount/getAllAccount")
// 	public ResponseEntity<?> getAllAccount(HttpServletRequest request, HttpServletResponse response) throws JsonMappingException, JsonProcessingException {
// 	//create json response	
//     Map<String, Object> jsonResponse = new HashMap<String, Object>();
//     HttpHeaders headers = new HttpHeaders();
//     headers.setContentType(MediaType.APPLICATION_JSON);

//     List<Account> accounts=accountService.getAllAccounts();
    
//     jsonResponse.put("response", "Account found");
//     jsonResponse.put("menu", accounts);
//     return new ResponseEntity<>(jsonResponse, headers, HttpStatus.OK);
//   }


//   // =========================================== 	DELETE /manageMenu/delete ===============================


//   @DeleteMapping("/manageAccount/deleteAccount/{id}")
//   public  ResponseEntity<?> deleteMenu(HttpServletRequest request, HttpServletResponse response,
//                                       @PathVariable("id") String id) {
     
//       //create json response	
//       Map<String, Object> jsonResponse = new HashMap<String, Object>();
//       HttpHeaders headers = new HttpHeaders();
//       headers.setContentType(MediaType.APPLICATION_JSON);

//       if(accountService.getAccount(id)==null){
//         jsonResponse.put("response", "Menu not found");
//         return new ResponseEntity<>(jsonResponse, headers, HttpStatus.NOT_FOUND);
//       }else{
//         accountService.deleteAccount(id);
//         jsonResponse.put("response", "deleted");
//         return new ResponseEntity<>(jsonResponse, headers, HttpStatus.OK);
//       }                
//   }



//   // =========================================== 	DELETE /manageMenu/deleteAll ===============================

//   @DeleteMapping("/manageAccount/deleteAllAccount")
//   public  ResponseEntity<?> deleteAllAccount(HttpServletRequest request, HttpServletResponse response ){
     
//       //create json response	
//       Map<String, Object> jsonResponse = new HashMap<String, Object>();
//       HttpHeaders headers = new HttpHeaders();
//       headers.setContentType(MediaType.APPLICATION_JSON);

//         accountService.deleteAllAccount();
//         jsonResponse.put("response", "deleted all");
//         return new ResponseEntity<>(jsonResponse, headers, HttpStatus.OK);
                   
//   }




//   // =========================================== 	PATCH /manageAccount/updateAccount ===============================




//   @PatchMapping("/manageAccount/updateAccount/{id}")
//   public  ResponseEntity<?> updateMenu(HttpServletRequest request, HttpServletResponse response,
//                                         @PathVariable("id") String id,
//                                         @RequestParam("userName") String userName,
//                                         @RequestParam("password") String password,
//                                         @RequestParam("role") String role) throws IOException {

// 		//create json response
// 		Map<String, Object> jsonResponse = new HashMap<String, Object>();
// 		//create http headers
// 		HttpHeaders headers = new HttpHeaders();
// 		headers.setContentType(MediaType.APPLICATION_JSON);
		
// 		// if (userName==null || password==null || role==null) {
// 		// 	jsonResponse.put("response", "All field are needed");
// 		// 	return new ResponseEntity<>(jsonResponse, headers, HttpStatus.BAD_REQUEST);
// 		// }
//     Account account =accountService.getAccount(id);
// 		if(account==null){
// 			jsonResponse.put("response", "Menu doesn't exist");
// 			return new ResponseEntity<>(jsonResponse, headers, HttpStatus.NOT_FOUND);
// 		}
//     	accountService.update(id ,userName, password, role);
//       	jsonResponse.put("response", "modified");
//       	return new ResponseEntity<>(jsonResponse, headers, HttpStatus.OK);

//   }


	
}
