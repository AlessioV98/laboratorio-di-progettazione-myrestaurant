package edu.myRestaurant.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import edu.myRestaurant.domain.ImageGallery;

public interface ImageGalleryRepository extends JpaRepository< ImageGallery, String>{

    /** 
 	* This method returns true iff that email exists in AccountRepository.
 	* @param String email
 	*/
     
	boolean existsByid(String id);
    
}
