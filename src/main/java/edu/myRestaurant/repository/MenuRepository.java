package edu.myRestaurant.repository;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import edu.myRestaurant.domain.Menu;

public interface MenuRepository extends JpaRepository<Menu,String>{
	

    Menu findByTitle(String title);

    boolean existsMenuByTitle(String title); 

    List<Menu> findMenuByDishesId(String id);

}