package edu.myRestaurant.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import edu.myRestaurant.domain.Dish;

public interface DishRepository extends JpaRepository<Dish, String>{

    public Dish findByName(String name);

    public boolean existsByName(String name);
    
}
