package edu.myRestaurant.repository;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import javax.transaction.Transactional;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import org.springframework.data.repository.query.Param;

import edu.myRestaurant.domain.Reservation;

public interface ReservationRepository extends JpaRepository<Reservation, Long>{
	
	/** 
 	* This method returns true iff that email exists in ReservationRepository.
 	* @param String email
 	*/
	boolean existsByEmail(String email);


	/** 
 	* This method returns the Reservation that matches that email.
 	* @param String email
 	*/
	@Query("select r from Reservation r where r.email = :email")
	Reservation findByEmail(@Param("email")String email);
	

	/** 
 	* This method returns the Reservation that matches that id.
 	* @param String id
 	*/
	@Query("select r from Reservation r where r.id = :id")
	Reservation findById(@Param("id")String id);


	/** 
 	* This method saves an Reservation in ReservationRepository.
 	* @param Reservation res
 	*/
	 Reservation save(Reservation res);



	/** 
 	* This method updates the Reservation that matches that id.
	* @param String id	 
 	* @param String email
	* @param String tel
	* @param LocalDateTime datetime
	* @param int people
	* @param int tablenumber
 	*/
	@Modifying
    @Transactional
    @Query("update Reservation r set r.email=:email, r.tel=:tel, r.datetime=:datetime, r.people=:people, r.tablenumber=:tablenumber where r.id=:id")
    void setReservationById(@Param("id")String id, @Param("email")String email, @Param("tel")String tel, @Param("datetime")LocalDateTime datetime, @Param("people")int people, @Param("tablenumber")int tablenumber);
	


	/** 
 	* This method deletes the Reservation that matches that id.
	* @param String id
 	*/
	@Modifying	
    @Transactional
	@Query("delete Reservation r where r.id= :id")
	void deleteById(String id);

	/** 
 	* This method returns the number of people reserved for that day splitting them in launch and dinner.
	* @param LocalDateTime start
	* @param LocalDateTime end
 	*/

	@Transactional
	@Query("select SUM(people) from Reservation r where r.datetime between :start and :end")
	Integer getPeopleByDay(@Param("start")LocalDateTime start, @Param("end")LocalDateTime end);

	/** 
 	* This method returns the reservation for that day splitting them in launch and dinner.
	* @param LocalDate date
 	*/

	@Transactional
	@Query("select r from Reservation r where r.datetime like CONCAT('%',:date,'%')")
    List<Reservation> getReservationByDay(LocalDate date);

	@Transactional
	List<Reservation> findAllByEmail(String email);
	
}
