package edu.myRestaurant.repository;
import javax.transaction.Transactional;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import org.springframework.data.repository.query.Param;

import edu.myRestaurant.domain.Account;

public interface AccountRepository extends JpaRepository<Account, Long>{
	
	/** 
 	* This method returns true iff that email exists in AccountRepository.
 	* @param String email
 	*/
	boolean existsByEmail(String email);


	/** 
 	* This method returns the Account that matches that email.
 	* @param String email
 	*/
	@Query("select a from Account a where a.email = :email")
	Account findByEmail(@Param("email")String email);
	

	/** 
 	* This method returns the Account that matches that id.
 	* @param String id
 	*/
	@Query("select a from Account a where a.id = :id")
	Account findById(@Param("id")String id);


	/** 
 	* This method saves an Account in AccountRepository.
 	* @param Account account
 	*/
	Account save(Account account);



	/** 
 	* This method updates the Account that matches that id.
	* @param String id	 
 	* @param String email
	* @param String name
	* @param String password
	* @param String surname
	* @param String role
	* @param String tel
 	*/
	@Modifying
    @Transactional
    @Query("update Account a set a.email=:email, a.name=:name, a.password=:password, a.surname=:surname, a.role=:role, a.tel=:tel where a.id=:id")
    void setAccountById(@Param("id")String id, @Param("email")String email, @Param("name")String name, @Param("password") String password, @Param("surname") String surname, @Param("role") String role,  @Param("tel")String tel);
	


	/** 
 	* This method deletes the Account that matches that id.
	* @param String id
 	*/
	@Modifying	
    @Transactional
	@Query("delete Account a where a.id= :id")
	void deleteById(String id);

	
}
