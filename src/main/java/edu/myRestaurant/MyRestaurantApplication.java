package edu.myRestaurant;

import java.util.TimeZone;

import javax.annotation.PostConstruct;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MyRestaurantApplication {
	@PostConstruct
  	public void init(){
    // Setting Spring Boot SetTimeZone
    	TimeZone.setDefault(TimeZone.getTimeZone("Europe/Rome"));
  	}
	public static void main(String[] args) {
		SpringApplication.run(MyRestaurantApplication.class, args);
	}

}
