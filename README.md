# Progetto MyRestaurant


# Introduction
This project aims to create a web-app that can be used for the creation and its management of advertising information, and of possible services that can be provided for an audience of possible customers of a catering company.



# Cloud-native application
Our application will feature cloud-native features.
Then we will use the DevOps development process, specifying pipelines and stages that allow us to achieve the goal of continuous integration and continuous distribution [CI / CD] (# cicd). The system represents a MONOLITHIC architecture which will then be containerized through [Docker] (https://www.docker.com/) for their distribution in the deploying phase.
 
# External service
Within the system to improve the service offered by the application we have decided to add two external services, which are contacted by our system via API. The first service is that of [google Maps] (https://mapsplatform.google.com/) which allows us to add the google map in our UX, and consequently to allow customers to start a road navigation from the phone , and observe some characteristic points of the landscape, with indications set for cyclists / pedestrians.
The second service is the one offered by [OpenWeatherApi] (https://openweathermap.org/api) which provides weather forecasts, we use their bees to show the current weather and that of the next two days.


# Architettura software
![Architectural spike](/Readme-images/LogicalViewArchitecture.JPG)

# SpringSecurity
![Spring Security](/Readme-images/SpringSecurity.JPG)

# Data Model
![Data Model](/Readme-images/DataModel.JPG)

# Use Cases
![Use Cases](/Readme-images/UseCases.JPG)


# Git Branch

- main (commit comment only 0.0)
- hotix  (commit comment type test pass)
- sprint (commit comment : bug fixed , future complete) can only start from integration
- "particularFeatures"  (can only sart from development)


<a name="DevOps"></a> 
# DevOps
![DevOps Tools](/Readme-images/DevOps-Tools.JPG)




<a name="cicd"></a> 
# CI/CD Pipelines
The stages / jobs chosen for our pipeline (CI / CD) are shown below:

- **Build**
- **Verify**
- **Package**
- **Release**
- **Deploy**
- **Documentation**



# Global Definition
Global environment variables available within each job:

```maven
variables:
 # SONAR_USER_HOME: "${CI_PROJECT_DIR}/.sonar"  # Defines the location of the analysis task cache
  SONAR_USER_HOME: "%CI_PROJECT_DIR%\ .sonar"  # Defines the location of the analysis task cache
  GIT_DEPTH: "0"  # Tells git to fetch all the branches of the project, required by the analysis task
```


## Build
The build of the project is realized using the special goal **compile** of Maven:
```maven
mvn clean compile
```




## Verify 
[![SonarCloud](https://sonarcloud.io/images/project_badges/sonarcloud-white.svg)](https://sonarcloud.io/summary/new_code?id=AlessioV98_laboratorio-di-progettazione-myrestaurant)
In this particular stage we use [SonarCloud](https://sonarcloud.io/) that is a cloud-based code analysis service designed to detect code quality issues , continuously ensuring the maintainability, reliability and security of code.

Maven command:
```maven
mvn verify sonar:sonar -Dsonar.projectKey=AlessioV98_laboratorio-di-progettazione-myrestaurant
```
In this step we take advantage of a cache to allow communication between different jobs, independent of each other, guaranteeing the **save-restore** of the latter in subsequent steps, this not only improves the efficiency of the single job but allows to do not re-execute instructions that would be redundant on this stage.

```maven
cache:
    key: "${CI_JOB_NAME}"
    paths:
     - .sonar/cache
```


# Test

A unit test aims to verify the correct functioning of only one module within a system, such as a single method or function.
Since Maven's goal **verify** implicitly also carries out the test phase (both unit and integration) and package we decided to exploit the latter to fulfill our purpose.




## Karma
Karma is a node-based test tool that allows we to test our JavaScript codes across multiple real browsers. A node-based tool is any tool that needs the Nodejs engine installed for it to run and can be accessed (installed) through the node package manager (npm).

```json
  "devDependencies": {
    "karma": "~6.3.0",
    "karma-chai": "^0.1.0",
    "karma-chrome-launcher": "~3.1.0",
    "karma-coverage": "~2.1.0",
    "karma-jasmine": "~4.0.0",
    "karma-jasmine-html-reporter": "~1.7.0",
    "karma-mocha": "^2.0.1",
  }
```
The Karma.conf.js configuration file should contain the setup instruction that Karma must follow to perform the three important functions of Karma.
This configuration file can be created manually or automatically by using the command:```karma init```, which starts to display different questions for you to answer, and Karma uses the answers that you provide to set up the configuration file.

The tests are performed without the aid of the browser, because not compatible with testing in container , so we adopt HeadLessChrome using the following command```ng test --watch=false --browsers=HeadlessChrome```.



## JUnit Mockito
JUnit and mockito are classified as Testing Framework tools.
Mockito is a mocking framework. It is a Java-based library used to create simple and basic test APIs for performing unit testing of Java applications. It can also be used with other frameworks such as JUnit

Mocking is a process of developing the objects that act as the mock or clone of the real objects. In other words, mocking is a testing technique where mock objects are used instead of real objects for testing purposes. Mock objects provide a specific (dummy) output for a particular (dummy) input passed to it.


# Package
In the package stage all the files / libraries / dependencies used by the application are compressed into a single file with the .jar extension to facilitate its distribution, as a single "compressed" file requires less effort to be transferred over the network and processed by a calculator.
Compression does not alter the structure of the files themselves and also disconnects the application from the native platform as long as a JVM is used for its execution.
In maven this is achieved through the special goal **package**:

```maven
mvn package
```
A single .jar file is then produced inside the target folder ready to be released and distributed.
```maven
paths:
    - target/*.jar
```


<a name="Containerized"></a>
# Release

During the release stage we rely on Docker, in particular on [Docker Engine](https://docs.docker.com/engine/) for the automatic creation of an image (myRestaurant) starting from the instructions contained in the Dockerfile, the image is then pushed to the public gusmero repository.
Inside the Dockerfile the command is defined as ENTRYPOINT
```java
java -jar "LandingPage-0.0.1-SNAPSHOT.jar"
```
to run the application when the container is started, through .jar obtained in the package stage, and the EXPOSE command to expose port 8080 inside the container.
Thanks to the operating system level virtualization services offered by Docker (Platform as a Service) we are able to have a container for our application that can be run on any server.

Connection to GitLab Container Registry:
```docker
docker login -u $DOCKER_HUB_ID -p $DOCKER_HUB_TOKEN
docker build -t $DOCKER_IMAGE -f ./Dockerfile .
docker tag $DOCKER_IMAGE $DOCKER_REPO_IMAGE:1.0
docker push $DOCKER_REPO_IMAGE:1.0
```



# Front end with angular
Angular is a development platform, built on TypeScript. As a platform, Angular includes:
A component-based framework for building scalable web applications
A collection of well-integrated libraries that cover a wide variety of features, including routing, forms management, client-server communication, and more
A suite of developer tools to help you develop, build, test, and update your code.






# Frontend maven plugin
we use [Frontend maven plugin](https://github.com/eirslett/frontend-maven-plugin) for build ,test and package for both backend and frontend. 
The following code is isert into pom.xml 
```xml
<plugin>
				<groupId>com.github.eirslett</groupId>
				<artifactId>frontend-maven-plugin</artifactId>
				<version>1.12.1</version>
				<configuration>
					<workingDirectory>src/main/frontend</workingDirectory>
				</configuration>

				<executions>
					<execution>
						<id>install-node-and-npm</id>
						<goals>
							<goal>install-node-and-npm</goal>
						</goals>
						<configuration>
							<nodeVersion>v12.20.0</nodeVersion>
						</configuration>
					</execution>
					<execution>
						<id>npm install</id>
						<goals>
							<goal>npm</goal>
						</goals>
						<configuration>
							<arguments>install</arguments>
						</configuration>
					</execution>
					<execution>
						<id>npm build</id>
						<goals>
							<goal>npm</goal>
						</goals>
						<configuration>
							<arguments>run build</arguments>
						</configuration>
						<phase>generate-resources</phase>
					</execution>
					
					<execution>
						<id>npm test</id>
						<goals>
							<goal>npm</goal>
						</goals>
						<phase>test</phase>
					</execution>
				</executions>
			</plugin>
			<plugin>
				<artifactId>maven-resources-plugin</artifactId>
				<executions>
					<execution>
						<id>copy-resources</id>
						<phase>validate</phase>
						<goals>
							<goal>copy-resources</goal>
						</goals>
						<configuration>
							<outputDirectory>${project.build.directory}/classes/static/</outputDirectory>
							<resources>
								<resource>
									<directory>/src/main/frontend/dist</directory>
								</resource>
							</resources>
						</configuration>
					</execution>
				</executions>
			</plugin>
```


## Deploy on Heroku
In this section we will describe deploying configuration for push the entire repository into [heroku](https://dashboard.heroku.com/apps). Heroku is a Platform as Service usefull for pushing webb application with no cost. The application as a public domain that can be achived through this [myRestaurant](https://laboratorio-di-progettazione.herokuapp.com/). For the db connection i use ClearDB.

- create a heroku app
- add ClearMysqlDB
- add deployment stage like below
- add two Gitlab CI/CD variable $HEROKU_APP(application name) $HEROKU_API_KEY(achived on Account Setting , ApiKey)
- add procfile file containing web: java -jar target/LaboratorioDiProgettazione-0.0.1-SNAPSHOT.jar , (started command in heroku)
- add a system.properties file containing java.runtime.version=11
- modified application.properties


```yaml
# HEROKU DEPLOYMENT
#username=b25fd10ebf11e3
#password=c13c0f16
#host=eu-cdbr-west-02.cleardb.net
#database=heroku_66c09981a98f4d2
#porta=3306
server.port=${PORT:8080}
spring.datasource.url=jdbc:mysql://b6527ca5756cf1:6ce52772@eu-cdbr-west-02.cleardb.net/heroku_baccd714f1bfd55?reconnect=true
```

```yaml
#DEPLOY 
deploy:
  stage: deploy
  image: ruby:2.6
  script:
    - echo "Deploying application..."
    - apt-get update -qy
    - apt-get install -y ruby-dev
    - gem install dpl
    - dpl --provider=heroku --app=$HEROKU_APP --api-key=$HEROKU_API_KEY
    - echo "Deploying completed..."
  only:
    - master
```
Usefull command for heroku cli  
```cli
heroku login
heroku logs --app=myrestaurant-lab
```


# Documentation 
**Doxygen** is a tool or command line-based documentation generator that helps in writing reference documentation for software. As it is written within the code, it is very easy to keep on updating. Moreover, it can cross-reference the code and documentation, which helps in referring to the actual code. 


**Compodoc** is a documentation tool for Angular applications. It generates a static documentation of your application.